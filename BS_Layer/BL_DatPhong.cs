﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	public class DatPhong
	{
		public int MaDatPhong { get; set; }
		public int MaKH { get; set; }
		public string MaPhong { get; set; }
		public DateTime NgayDen { get; set; }
		public DateTime NgayDi { get; set; }
		public int SoNguoiO { get; set; }
		public float TienCoc { get; set; }
		public int NVTiepNhan { get; set; }
		public float TongTien { get; set; }
		public int SoNgayO { get; set; }
		public bool TraPhong { get; set; }
	}
	internal class BL_DatPhong
	{
		DBMain db = null;
		public BL_DatPhong()
		{
			db = new DBMain();
		}
		public DataTable LSDatPhong()
		{
			string query = "select MaDatPhong, HoTen, MaPhong, NgayDen, NgayDi, SoNgayO, TraPhong from DatPhong, KhachHang where DatPhong.MaKH = KhachHang.MaKH";
			return db.ExecuteQueryDataTable(query);
		}
	}
}
