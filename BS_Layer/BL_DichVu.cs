﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	public class ChiTietDichVu
	{
		public int MaDV { get; set; }
		public string TenDV { get; set; }
		public float Gia { get; set; }
		public int SoLuong { get; set; }
	}
	public class DichVu
	{
		public int MaDV { get; set; }
		public string TenDV { get; set; }
		public float Gia { get; set; }
	}
	internal class BL_DichVu
	{
		DBMain db = null;
		public BL_DichVu()
		{
			db = new DBMain();
		}
		public DataTable DSDichVu()
		{
			return db.ExecuteQueryDataTable("select * from DichVu");
		}
		public DataTable TimKiemDichVu(string text)
		{
			string query = "select * from DichVu where MaDV like '%" + text + "%' OR TenDV like '%" + text + "%' OR Gia like '%" + text + "%'";
			return db.ExecuteQueryDataTable(query);
		}
		public void TaoDichVu(string tenDV, float gia)
		{
			string query = "insert into DichVu values(N'" + tenDV + "', '" + gia + "')";
			db.MyExecuteNonQuery(query);
		}
		public void SuaDichVu(int maDV, string tenDV, float gia)
		{
			string query = "update DichVu set TenDV=N'" + tenDV + "', Gia='" + gia + "' where MaDV=" + maDV;
			db.MyExecuteNonQuery(query);
		}
		public void XoaDichVu(int maDV)
		{
			string query = "delete from DichVu where MaDV=" + maDV;
			db.MyExecuteNonQuery(query);
		}
		public List<ChiTietDichVu> ListDichVu(int madatphong)
		{
			List<ChiTietDichVu> ldv = new List<ChiTietDichVu>();
			string query = "select ChiTietDatPhong.MaDV, TenDV, SoLuong, ThanhTien from DatPhong, ChiTietDatPhong, DichVu where DatPhong.MaDatPhong = ChiTietDatPhong.MaDatPhong AND DatPhong.MaDatPhong=" + madatphong + " AND ChiTietDatPhong.MaDV = DichVu.MaDV";
			DataTable dt = db.ExecuteQueryDataTable(query);
			int i = 0;
			foreach (DataRow row in dt.Rows)
			{
				ChiTietDichVu ctdv = new ChiTietDichVu();
				ctdv.MaDV = Convert.ToInt32(row.ItemArray[0]);
				ctdv.TenDV = row.ItemArray[1].ToString();
				ctdv.SoLuong = Convert.ToInt32(row.ItemArray[2]);
				ctdv.Gia = (float)Convert.ToDouble(row.ItemArray[3].ToString());
				ldv.Add(ctdv);
				i++;
			}
			return ldv;
		}
	}
}
