﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	public class KhachHang
	{
		public int MaKH { get; set; }
		public string HoTen { get; set; }
		public string Email { get; set; }
		public string SDT { get; set; }
		public string CCCD { get; set; }
		public string DiaChi { get; set; }
		public int SoLanNghi { get; set; }
		public bool KhachVIP { get; set; }
	}
	internal class BL_KhachHang
	{
		DBMain db = null;
		public BL_KhachHang()
		{
			db = new DBMain();
		}
		public DataTable DanhSachKhachHang()
		{
			string query = "select * from KhachHang";
			return db.ExecuteQueryDataTable(query);
		}
		public DataTable TimKiemKhachHang(string text)
		{
			string query = "select * from KhachHang where HoTen like '%" + text + "%' OR DiaChi like '%" + text + "%' OR SDT like '%" + text + "%' OR Email like '%" + text + "%' OR CCCD like '%" + text + "%'";
			return db.ExecuteQueryDataTable(query);
		}
		public KhachHang TimKiemKhachHang_MaKH(int MaKH)
		{
			string query = "select * from KhachHang where MaKH=" + MaKH;
			List<string> l_kh = db.ExecuteQueryRecord(query);
			KhachHang kh = new KhachHang();
			kh.MaKH = Convert.ToInt32(l_kh[0].ToString());
			kh.HoTen = l_kh[1].ToString();
			kh.DiaChi = l_kh[2].ToString();
			kh.SDT = l_kh[3].ToString();
			kh.Email = l_kh[4].ToString();
			kh.CCCD = l_kh[5].ToString();
			kh.SoLanNghi = Convert.ToInt32(l_kh[6].ToString());
			kh.KhachVIP = Convert.ToBoolean(l_kh[7].ToString());
			return kh;
		}
		public KhachHang ThemKhachHang(string HoTen, string DiaChi, string Email, string SDT, string CCCD)
		{
			List<string> kh = db.ExecuteQueryRecord("select * from KhachHang where CCCD='" + CCCD + "'");
			if (kh.Count > 0)
			{
				int makh = Convert.ToInt32(kh[0]);
				int sln = Convert.ToInt32(kh[6]);
				if (sln == 2)
				{
					db.MyExecuteNonQuery("update KhachHang set SoLanNghi='3' where MaKH='" + makh + "'");
					db.MyExecuteNonQuery("update KhachHang set KhachVIP='1' where MaKH='" + makh + "'");
				}
				else
				{
					db.MyExecuteNonQuery("update KhachHang set SoLanNghi='" + (sln + 1) + "' where MaKH='" + makh + "'");
				}
			}
			else
			{
				db.MyExecuteNonQuery("insert into KhachHang values(N'" + HoTen + "', N'" + DiaChi + "', '" + SDT + "', '" + Email + "', '" + CCCD + "', '1', '0')");
			}
			string query_laykhachhang = "select top 1 * from KhachHang order by MaKH desc";
			List<string> l_khachhang = db.ExecuteQueryRecord(query_laykhachhang);
			KhachHang khachhang = new KhachHang();
			khachhang.MaKH = Convert.ToInt32(l_khachhang[0].ToString());
			khachhang.HoTen = l_khachhang[1].ToString();
			khachhang.DiaChi = l_khachhang[2].ToString();
			khachhang.SDT = l_khachhang[3].ToString();
			khachhang.Email = l_khachhang[4].ToString();
			khachhang.CCCD = l_khachhang[5].ToString();
			khachhang.SoLanNghi = Convert.ToInt32(l_khachhang[6].ToString());
			khachhang.KhachVIP = Convert.ToBoolean(l_khachhang[7].ToString());
			return khachhang;
		}
		public void SuaKhachHang(int MaKH, string HoTen, string DiaChi, string Email, string SDT, string CCCD)
		{
			string query = "update KhachHang set HoTen=N'" + HoTen + "', DiaChi=N'" + DiaChi + "', Email=N'" + Email + "', SDT='" + SDT + "', CCCD='" + CCCD + "' where MaKH='" + MaKH + "'";
			db.MyExecuteNonQuery(query);
		}
		public void XoaKhachHang(int MaKH)
		{
			string query_deleteForeignKey = "update DatPhong set MaKH=null where MaKH=" + MaKH;
			db.MyExecuteNonQuery(query_deleteForeignKey);
			string query = "delete from KhachHang where MaKH=" + MaKH;
			db.MyExecuteNonQuery(query);
		}
	}
}
