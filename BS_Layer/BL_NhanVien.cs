﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	public class NhanVien
	{
		public int MaNV { get; set; }
		public string HoTen { get; set; }
		public string DiaChi { get; set; }
		public string SDT { get; set; }
		public string Email { get; set; }
		public string MatKhau { get; set; }
		public string ChucVu { get; set; }
	}
	class BL_NhanVien
	{
		DBMain db = null;
		public BL_NhanVien()
		{
			db = new DBMain();
		}
		public bool DangNhap(string Email, string Password, string ChucVu)
		{
			NhanVien n = this.LayNhanVien_Email(Email);
			if (n != null && n.ChucVu == ChucVu)
			{
				if (n.MatKhau == Password)
				{
					return true;
				}
			}
			return false;
		}

		public bool XacThucNhanVien(string Email, string SDT)
		{
			string query = "select MaNV, HoTen, DiaChi, SDT, Email, ChucVu from NhanVien where Email='" + Email + "' and SDT='" + SDT + "'";
			List<string> list = db.ExecuteQueryRecord(query);
			return list.Count > 0;
		}

		public bool DoiMatKhau(string Email, string MatKhauMoi)
		{
			string query = "Update NhanVien set MatKhau='" + MatKhauMoi + "' where Email='" + Email + "'";
			db.MyExecuteNonQuery(query);
			return true;
		}

		public DataTable LayDSNhanVien()
		{
			return db.ExecuteQueryDataTable("select MaNV, HoTen, DiaChi, SDT, Email, ChucVu from NhanVien");
		}

		public NhanVien LayNhanVien_Email(string Email)
		{
			NhanVien nv = new NhanVien();
			string query = "select MaNV, HoTen, DiaChi, SDT, Email, MatKhau, ChucVu from NhanVien where NhanVien.Email = '" + Email + "'";
			List<string> reader = db.ExecuteQueryRecord(query);
			if (reader.Count == 0)
			{
				return null;
			}
			nv.MaNV = Convert.ToInt32(reader[0]);
			nv.HoTen = reader[1];
			nv.DiaChi = reader[2];
			nv.SDT = reader[3];
			nv.Email = reader[4];
			nv.MatKhau = reader[5];
			nv.ChucVu = reader[6];
			return nv;
		}
		public NhanVien LayNhanVien_MaNV(int MaNV)
		{
			NhanVien nv = new NhanVien();
			string query = "select MaNV, HoTen, DiaChi, SDT, Email, ChucVu from NhanVien where NhanVien.MaNV = '" + MaNV.ToString() + "'";
			List<string> reader = db.ExecuteQueryRecord(query);
			if (reader.Count == 0)
			{
				return null;
			}
			nv.MaNV = Convert.ToInt32(reader[0]);
			nv.HoTen = reader[1];
			nv.DiaChi = reader[2];
			nv.SDT = reader[3];
			nv.Email = reader[4];
			nv.ChucVu = reader[5];
			return nv;
		}

		public void ThemNhanVien(string hoten, string diachi, string email, string sdt, string matkhau, string chucvu)
		{
			string query = "Insert into NhanVien values(" + "N'" + hoten + "',N'" + diachi + "',N'" + sdt + "',N'" + email + "',N'" + matkhau + "',N'" + chucvu + "')";
			db.MyExecuteNonQuery(query);
		}

		public void SuaNhanVien(int manv, string hoten, string diachi, string email, string sdt, string matkhau, string chucvu)
		{
			string query = "Update NhanVien Set HoTen=N'" + hoten + "', DiaChi=N'" + diachi + "', SDT='" + sdt + "', Email=N'" + email + "', MatKhau=N'" + matkhau + "', ChucVu=N'" + chucvu + "' where MaNV='" + manv + "'";
			db.MyExecuteNonQuery(query);
		}

		public void XoaNhanVien(int manv)
		{
			string query = "Delete NhanVien where MaNV=" + manv;
			db.MyExecuteNonQuery(query);
		}

		public DataTable TimKiemNhanVien(string text)
		{
			string query = "select MaNV, HoTen, DiaChi, SDT, Email, ChucVu from NhanVien where HoTen like '%" + text + "%' OR DiaChi like '%" + text + "%' OR SDT like '%" + text + "%' OR Email like '%" + text + "%' OR ChucVu like '%" + text + "%'";
			return db.ExecuteQueryDataTable(query);
		}
	}
}
