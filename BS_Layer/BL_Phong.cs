﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	public class MyPhong
	{
		public string MaPhong { get; set; }
		public int TinhTrang { get; set; }
		public float Gia { get; set; }
		public int SucChua { get; set; }
		public string MoTa { get; set; }
		public string TenLoaiPhong { get; set; }
	}
	public class HoaDon
	{
		public int MaHoaDon { get; set; }
		public int MaDatPhong { get; set; }
		public DateTime NgayLap { get; set; }
		public float TongTien { get; set; }
		public int NVThanhToan { get; set; }
	}
	class BL_Phong
	{
		DBMain db = null;
		public BL_Phong()
		{
			db = new DBMain();
		}
		public List<MyPhong> LayDSPhong()
		{
			List<MyPhong> list = new List<MyPhong>();
			string query = "select Phong.MaPhong, Phong.TinhTrang, LoaiPhong.Gia, LoaiPhong.SucChua, LoaiPhong.MoTa, LoaiPhong.TenLoaiPhong from Phong, LoaiPhong where Phong.MaLoaiPhong = LoaiPhong.MaLoaiPhong";
			DataTable dt = db.ExecuteQueryDataTable(query);
			foreach (DataRow dr in dt.Rows)
			{
				MyPhong mp = new MyPhong();
				mp.MaPhong = dr.ItemArray[0].ToString();
				mp.TinhTrang = Convert.ToInt32(dr.ItemArray[1].ToString());
				mp.Gia = (float)Convert.ToDouble(dr.ItemArray[2].ToString());
				mp.SucChua = Convert.ToInt32(dr.ItemArray[3].ToString());
				mp.MoTa = dr.ItemArray[4].ToString();
				mp.TenLoaiPhong = dr.ItemArray[5].ToString();
				list.Add(mp);
			}
			return list;
		}
		public void TraPhong(string maphong)
		{
			// Chuyển tình trạng về 3 (đang dọn)
			string query_tinhtrang = "update Phong set Phong.TinhTrang = 3 from Phong, LoaiPhong where Phong.MaLoaiPhong = LoaiPhong.MaLoaiPhong and Phong.MaPhong = '" + maphong + "'";
			db.MyExecuteNonQuery(query_tinhtrang);
			// Gán TraPhong=True
			string query_traphong = "update DatPhong set TraPhong = 1 where DatPhong.MaPhong = '" + maphong + "'";
			db.MyExecuteNonQuery(query_traphong);
		}
		public void DonPhong(string maphong)
		{
			// Chuyển tình trạng về 1 (sẵn sàng)
			string query = "update Phong set Phong.TinhTrang = 1 where Phong.MaPhong = '" + maphong + "'";
			db.MyExecuteNonQuery(query);
		}
		public void ThemDichVu(int maDatPhong, int madv, int soLuong)
		{
			// Chi tiết dịch vụ
			string query_dichvu = "select * from DichVu where MaDV=" + madv;
			List<string> dv = db.ExecuteQueryRecord(query_dichvu);
			float gia = (float)Convert.ToDouble(dv[2].ToString());
			float tongTien = gia * soLuong;
			string query_ctdp = "insert into ChiTietDatPhong values('" + maDatPhong + "', '" + madv + "', '" + soLuong + "', '" + tongTien + "')";
			db.MyExecuteNonQuery(query_ctdp);

			// Tìm đặt phòng
			string query_dp = "select * from DatPhong where DatPhong.MaDatPhong=" + maDatPhong;
			List<string> dp = db.ExecuteQueryRecord(query_dp);

			// Cập nhật tổng tiền cho đặt phòng
			float gia_capnhat = (float)Convert.ToDouble(dp[8].ToString()) + tongTien;
			string query_capnhatgia = "update DatPhong set TongTien='" + gia_capnhat + "' where DatPhong.MaDatPhong='" + maDatPhong + "'";
			db.MyExecuteNonQuery(query_capnhatgia);
		}
		public void DatPhong(int maKH, string maPhong, DateTime ngayDen, DateTime ngayDi, int soNguoiO, float tienCoc, int NVTiepNhan, int soNgayO)
		{
			// Lấy giá phòng
			string query_giaphong = "select Gia from Phong, LoaiPhong where Phong.MaLoaiPhong = LoaiPhong.MaLoaiPhong and Phong.MaPhong='" + maPhong + "'";
			List<string> gia = db.ExecuteQueryRecord(query_giaphong);

			// Tổng tiền hiện tại = Số ngày ở * Giá phòng - Tiền cọc
			float tongTien = soNgayO * (float)Convert.ToDouble(gia[0].ToString()) - tienCoc;
			string query_dp = "insert into DatPhong values('" + maKH + "', '" + maPhong + "',' " + ngayDen + "','" + ngayDi + "','" + soNguoiO + "','" + tienCoc + "','" + NVTiepNhan + "','" + tongTien + "','" + soNgayO + "', 'false' )";
			db.MyExecuteNonQuery(query_dp);

			// Chuyển trạng thái phòng về 2 (đang thuê)
			string query_tinhtrang = "update Phong set Phong.TinhTrang = 2 where Phong.MaPhong = '" + maPhong + "'";
			db.MyExecuteNonQuery(query_tinhtrang);
		}
		public HoaDon XuatHoaDon(int madatphong, int makh, int manv)
		{
			// Gán ngày đi = hôm nay
			string query_ganngaydi = "update DatPhong set NgayDi='" + DateTime.UtcNow.Date + "' where MaDatPhong=" + madatphong;
			db.MyExecuteNonQuery(query_ganngaydi);

			// Tính số ngày ở thực tế
			string query_laydatphong = "select * from DatPhong where MaDatPhong=" + madatphong;
			List<string> datphong = db.ExecuteQueryRecord(query_laydatphong);
			int soNgayO = Convert.ToInt32(datphong[9].ToString());
			DateTime ngayDen = Convert.ToDateTime(datphong[3].ToString());
			DateTime ngayDi = Convert.ToDateTime(datphong[4].ToString());
			int songay = ((TimeSpan)(ngayDi - ngayDen)).Days + 1;
			// Nếu rời đi sớm hơn dự tính --> Vẫn tính tiền theo số ngày ban đầu
			if (songay < soNgayO)
			{
				songay = soNgayO;
			}

			// Lấy giá phòng
			string query_laygiaphong = "select Gia from DatPhong, Phong, LoaiPhong where DatPhong.MaPhong = Phong.MaPhong and Phong.MaLoaiPhong = LoaiPhong.MaLoaiPhong and DatPhong.MaDatPhong=" + madatphong;
			List<string> l_giaphong = db.ExecuteQueryRecord(query_laygiaphong);
			float giaphong = (float)Convert.ToDouble(l_giaphong[0].ToString());

			// Xóa tiền nghỉ gốc
			float tongTien = (float)Convert.ToDouble(datphong[8].ToString());
			tongTien -= giaphong * soNgayO;
			// Cộng tiền theo số ngày nghỉ thực tế
			tongTien += giaphong * songay;
			db.MyExecuteNonQuery("update DatPhong set TongTien='" + tongTien + "' where MaDatPhong=" + madatphong);

			// Lập hóa đơn
			string query_xuathoadon = "insert into HoaDon values('" + madatphong + "', '" + DateTime.UtcNow.Date + "','" + tongTien + "','" + manv + "')";
			db.MyExecuteNonQuery(query_xuathoadon);

			// return hóa đơn
			string query_layhoadon = "select top 1 * from HoaDon order by MaHoaDon desc";
			List<string> l_hoadon = db.ExecuteQueryRecord(query_layhoadon);
			HoaDon hd = new HoaDon();
			hd.MaHoaDon = Convert.ToInt32(l_hoadon[0].ToString());
			hd.MaDatPhong = Convert.ToInt32(l_hoadon[1].ToString());
			hd.NgayLap = Convert.ToDateTime(l_hoadon[2].ToString());
			hd.TongTien = (float)Convert.ToDouble(l_hoadon[3].ToString());
			hd.NVThanhToan = Convert.ToInt32(l_hoadon[4].ToString());
			return hd;
		}
		public DatPhong LayDatPhong(string maphong)
		{
			string query = "select top 1 MaDatPhong, MaKH, DatPhong.MaPhong, NgayDen, NgayDi, SoNguoiO, TienCoc, NVTiepNhan, TongTien, SoNgayO, TraPhong from DatPhong, Phong where DatPhong.MaPhong = '" + maphong + "' and DatPhong.MaPhong = Phong.MaPhong and Phong.TinhTrang=2 order by MaDatPhong desc";
			List<string> l_datphong = db.ExecuteQueryRecord(query);
			if (l_datphong.Count > 0)
			{
				DatPhong dp = new DatPhong();
				dp.MaDatPhong = Convert.ToInt32(l_datphong[0].ToString());
				dp.MaKH = Convert.ToInt32(l_datphong[1].ToString());
				dp.MaPhong = l_datphong[2].ToString();
				dp.NgayDen = Convert.ToDateTime(l_datphong[3].ToString());
				dp.NgayDi = Convert.ToDateTime(l_datphong[4].ToString());
				dp.SoNguoiO = Convert.ToInt32(l_datphong[5].ToString());
				dp.TienCoc = (float)Convert.ToDouble(l_datphong[6].ToString());
				dp.NVTiepNhan = Convert.ToInt32(l_datphong[7].ToString());
				dp.TongTien = (float)Convert.ToDouble(l_datphong[8].ToString());
				dp.SoNgayO = Convert.ToInt32(l_datphong[9].ToString());
				dp.TraPhong = Convert.ToBoolean(l_datphong[10].ToString());
				return dp;
			}
			return null;
		}
	}
}
