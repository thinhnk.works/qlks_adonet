﻿using QLKS_ADONET.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_ADONET.BS_Layer
{
	internal class BL_ThongKe
	{
		DBMain db = null;
		public BL_ThongKe()
		{
			db = new DBMain();
		}
		public float TongDoanhThu()
		{
			float tongDoanhThu = 0;
			DataTable ds_hoadon = db.ExecuteQueryDataTable("select * from HoaDon");
			if (ds_hoadon.Rows.Count == 0)
			{
				return 0;
			}
			foreach (DataRow row in ds_hoadon.Rows)
			{
				string query = "select * from DatPhong where MaDatPhong=" + row["MaDatPhong"].ToString();
				List<string> datphong = db.ExecuteQueryRecord(query);
				// Tiền cọc cho hóa đơn đó
				float tienCoc = (float)Convert.ToDouble(datphong[6].ToString());
				// Số tiền khách hàng trả khi check-out
				float tongTien = (float)Convert.ToDouble(row["TongTien"].ToString());
				// Doanh thu cho 1 hóa đơn = Tiền cọc + số tiền khách hàng trả khi check-out
				tongDoanhThu += (tongTien + tienCoc);
			}
			return tongDoanhThu;
		}
		public float DoanhThu1ThangGanNhat()
		{
			float tongDoanhThu = 0;
			// Lấy mốc tháng trước
			DateTime today = DateTime.Now.Date;
			DateTime lastmonth = today.AddMonths(-1);
			string date = lastmonth.ToString("yyyy-MM-dd");
			string query = "select * from HoaDon where NgayLap>='" + date + "'";
			DataTable ds_hoadon = db.ExecuteQueryDataTable(query);

			if (ds_hoadon.Rows.Count == 0)
			{
				return 0;
			}

			foreach (DataRow row in ds_hoadon.Rows)
			{
				string query_datphong = "select * from DatPhong where MaDatPhong=" + row["MaDatPhong"].ToString();
				List<string> datphong = db.ExecuteQueryRecord(query_datphong);
				// Tiền cọc cho hóa đơn đó
				float tienCoc = (float)Convert.ToDouble(datphong[6].ToString());
				// Số tiền khách hàng trả khi check-out
				float tongTien = (float)Convert.ToDouble(row["TongTien"].ToString());
				// Doanh thu cho 1 hóa đơn = Tiền cọc + số tiền khách hàng trả khi check-out
				tongDoanhThu += (tongTien + tienCoc);
			}
			return tongDoanhThu;
		}
		public Dictionary<string, float> DoanhThuTungPhong()
		{
			Dictionary<string, float> dttp = new Dictionary<string, float>();
			// Danh sách tên phòng
			List<string> list_tenphong = new List<string>() {
				"T101", "T102", "T103",
				"T104", "T105", "T106",
				"T107", "T108", "T109",
				"V201", "V202", "V203"
			};
			foreach (string tenphong in list_tenphong)
			{
				dttp[tenphong] = 0;
				float tien_tungphong = 0;
				// Lấy Tổng tiền và Tiền cọc theo tên phòng
				string query_layhoadon = "select HoaDon.TongTien, DatPhong.TienCoc from HoaDon, DatPhong where DatPhong.MaPhong = '" + tenphong + "'";
				DataTable dt_tien = db.ExecuteQueryDataTable(query_layhoadon);
				if (dt_tien.Rows.Count > 0)
				{
					foreach (DataRow row in dt_tien.Rows)
					{
						float tongTien = (float)Convert.ToDouble(row["TongTien"].ToString());
						float tienCoc = (float)Convert.ToDouble(row["TienCoc"].ToString());
						tien_tungphong += (tongTien + tienCoc);
					}
					dttp[tenphong] = tien_tungphong;
				}
			}
			return dttp;
		}
	}
}
