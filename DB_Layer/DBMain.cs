﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.DB_Layer
{
	class DBMain
	{
		string connectionString = "Data Source=(local); Initial Catalog=QuanLyKhachSan; Integrated Security=True";

		public DataTable ExecuteQueryDataTable(string strSQL)
		{
			DataTable dt = new DataTable();
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (SqlCommand cmd = new SqlCommand(strSQL, connection))
				{
					using (SqlDataAdapter da = new SqlDataAdapter(cmd))
					{
						da.Fill(dt);
					}
				}
			}
			return dt;
		}
		public List<string> ExecuteQueryRecord(string strSQL)
		{
			List<string> list = new List<string>();
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (SqlCommand cmd = new SqlCommand(strSQL, connection))
				{
					using (SqlDataReader reader = cmd.ExecuteReader())
					{
						if (reader.HasRows && reader.Read())
						{
							int i = 0;
							while (i < reader.FieldCount)
							{
								list.Add(reader[i].ToString());
								i++;
							}
						}
					}
				}
			}
			return list;
		}
		public void MyExecuteNonQuery(string strSQL)
		{
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (SqlCommand cmd = new SqlCommand(strSQL, connection))
				{
					try
					{
						cmd.ExecuteNonQuery();
					}
					catch (SqlException ex)
					{
						MessageBox.Show(ex.Message);
					}
				}
			}
		}
	}
}
