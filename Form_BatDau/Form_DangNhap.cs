﻿using QLKS_ADONET.BS_Layer;
using QLKS_ADONET.Form_NhanVien;
using QLKS_ADONET.Form_QuanLy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_BatDau
{
	public partial class Form_DangNhap : Form
	{
		public Form_DangNhap()
		{
			InitializeComponent();

		}
		private void btnClear_Click(object sender, EventArgs e)
		{
			txtPassword.Text = "";
			txtUsername.Text = "";
			txtUsername.Focus();
		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
			if (txtUsername.Text == "" || txtPassword.Text == "")
			{
				MessageBox.Show("Username and Password fileds are empty", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}
			else
			{
				BL_NhanVien blNhanVien = new BL_NhanVien();
				string ChucVu = (this.comboBox1.SelectedIndex == 0) ? "Admin" : "NhanVien";
				bool result = blNhanVien.DangNhap(this.txtUsername.Text.Trim(), txtPassword.Text.Trim(), ChucVu);
				if (result)
				{
					MessageBox.Show("OK");
					if (ChucVu == "NhanVien")
					{
						Form_DatPhong form = new Form_DatPhong();
						// Them Ho Ten Vao Form DatPhong
						BL_NhanVien BLnhanvien = new BL_NhanVien();
						NhanVien nhanVien = BLnhanvien.LayNhanVien_Email(txtUsername.Text.Trim());
						form.nhanVien = nhanVien;
						form.ShowDialog();
						txtUsername.Text = String.Empty;
						txtPassword.Text = String.Empty;
					}
					else
					{

						Form_Admin form = new Form_Admin();
						BL_NhanVien BLnhanvien = new BL_NhanVien();
						NhanVien nhanVien = BLnhanvien.LayNhanVien_Email(txtUsername.Text.Trim());
						form.nhanvien = nhanVien;
						form.ShowDialog();
						txtUsername.Text = String.Empty;
						txtPassword.Text = String.Empty;
					}
				}
				else
				{
					MessageBox.Show("Thông tin đăng nhập không chính xác");
				}
			}
		}

		private void checkbxShowPas_CheckedChanged(object sender, EventArgs e)
		{
			if (checkbxShowPas.Checked)
			{
				txtPassword.PasswordChar = '\0';
			}
			else
			{

				txtPassword.PasswordChar = '*';
			}
		}

		private void label6_Click(object sender, EventArgs e)
		{
			Form_QuenMatKhau formquenmatkhau = new Form_QuenMatKhau();
			formquenmatkhau.ShowDialog();
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void txtUsername_TextChanged(object sender, EventArgs e)
		{

		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void Form_DangNhap_Load(object sender, EventArgs e)
		{
			comboBox1.SelectedIndex = 1;
		}
	}
}
