﻿using QLKS_ADONET.BS_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_BatDau
{
	public partial class Form_QuenMatKhau : Form
	{
		bool verify = false;
		public Form_QuenMatKhau()
		{
			InitializeComponent();
			txtPassword.Enabled = false;
			txtComPassword.Enabled = false;
			btnChange.Enabled = false;
			btnClear.Enabled = false;
		}
		private void pictureBox1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void checkbxShowPas_CheckedChanged(object sender, EventArgs e)
		{
			if (checkbxShowPas.Checked == true)
			{
				txtPassword.PasswordChar = '\0';
				txtComPassword.PasswordChar = '\0';
			}
			else
			{
				txtPassword.PasswordChar = '*';
				txtComPassword.PasswordChar = '*';
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			txtComPassword.Text = "";
			txtPassword.Text = "";
			txtSDT.Text = "";
			txtEmail.Text = "";
			txtEmail.Focus();
		}

		private void label6_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnVerify_Click(object sender, EventArgs e)
		{
			if (txtEmail.Text == "" || txtSDT.Text == "")
			{
				MessageBox.Show("Email hoặc Số điện thoại đang bị trống", "Change Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				BL_NhanVien blnhanvien = new BL_NhanVien();
				bool result = blnhanvien.XacThucNhanVien(txtEmail.Text.Trim(), txtSDT.Text.Trim());
				if (result)
				{
					MessageBox.Show("Thông tin đối chiếu của bạn chính xác, Vui lòng điền mật khẩu mới", "Change Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.verify = true;
					txtPassword.Enabled = true;
					txtComPassword.Enabled = true;
					btnChange.Enabled = false;
					btnClear.Enabled = false;
					txtPassword.Focus();
				}
				else
				{
					MessageBox.Show("Email hoặc là số điện thoại không chín xác", "Change Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

			}
		}

		private void btnChange_Click(object sender, EventArgs e)
		{
			if (verify)
			{
				if (txtPassword.Text == "" || txtComPassword.Text == "")
				{
					MessageBox.Show("Vui lòng điền đầy đủ mật khẩu và mật khẩu xác nhận", "ChangePassword Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

				}
				else
				{
					BL_NhanVien blnhanvien = new BL_NhanVien();
					if (txtPassword.Text.Trim() == txtComPassword.Text.Trim())
					{
						bool result = blnhanvien.DoiMatKhau(txtEmail.Text.Trim(), txtPassword.Text.Trim());
						if (result)
						{
							MessageBox.Show("Đổi mật khẩu thành công", "ChangePassword Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show("Mật khẩu xác nhận lại không chính xác", "ChangePassword Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}

				}
			}
		}
	}
}
