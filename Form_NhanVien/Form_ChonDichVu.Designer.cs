﻿namespace QLKS_ADONET.Form_NhanVien
{
	partial class Form_ChonDichVu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnThemDichVu = new System.Windows.Forms.Button();
			this.nudSoLuong = new System.Windows.Forms.NumericUpDown();
			this.btnXacNhan = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudSoLuong)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(244, 35);
			this.panel1.TabIndex = 0;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox1.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox1.Location = new System.Drawing.Point(219, 7);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(20, 20);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnThemDichVu);
			this.panel2.Controls.Add(this.nudSoLuong);
			this.panel2.Controls.Add(this.btnXacNhan);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.dataGridView1);
			this.panel2.Location = new System.Drawing.Point(7, 41);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(231, 423);
			this.panel2.TabIndex = 1;
			this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
			// 
			// btnThemDichVu
			// 
			this.btnThemDichVu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(180)))), ((int)(((byte)(206)))));
			this.btnThemDichVu.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnThemDichVu.FlatAppearance.BorderSize = 3;
			this.btnThemDichVu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThemDichVu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThemDichVu.ForeColor = System.Drawing.Color.White;
			this.btnThemDichVu.Location = new System.Drawing.Point(5, 373);
			this.btnThemDichVu.Name = "btnThemDichVu";
			this.btnThemDichVu.Size = new System.Drawing.Size(220, 43);
			this.btnThemDichVu.TabIndex = 7;
			this.btnThemDichVu.Text = "Tạo Dịch Vụ";
			this.btnThemDichVu.UseVisualStyleBackColor = false;
			this.btnThemDichVu.Click += new System.EventHandler(this.btnThemDichVu_Click);
			// 
			// nudSoLuong
			// 
			this.nudSoLuong.AllowDrop = true;
			this.nudSoLuong.BackColor = System.Drawing.Color.White;
			this.nudSoLuong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudSoLuong.ForeColor = System.Drawing.Color.Black;
			this.nudSoLuong.Location = new System.Drawing.Point(169, 272);
			this.nudSoLuong.Name = "nudSoLuong";
			this.nudSoLuong.ReadOnly = true;
			this.nudSoLuong.Size = new System.Drawing.Size(56, 26);
			this.nudSoLuong.TabIndex = 6;
			this.nudSoLuong.Value = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.nudSoLuong.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// btnXacNhan
			// 
			this.btnXacNhan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnXacNhan.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnXacNhan.FlatAppearance.BorderSize = 3;
			this.btnXacNhan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXacNhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXacNhan.ForeColor = System.Drawing.Color.White;
			this.btnXacNhan.Location = new System.Drawing.Point(5, 322);
			this.btnXacNhan.Name = "btnXacNhan";
			this.btnXacNhan.Size = new System.Drawing.Size(220, 45);
			this.btnXacNhan.TabIndex = 4;
			this.btnXacNhan.Text = "Xác Nhận";
			this.btnXacNhan.UseVisualStyleBackColor = false;
			this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(5, 274);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(158, 22);
			this.label1.TabIndex = 3;
			this.label1.Text = "CHỌN SỐ LƯỢNG";
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
			this.dataGridView1.ColumnHeadersHeight = 25;
			this.dataGridView1.EnableHeadersVisualStyles = false;
			this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.dataGridView1.Location = new System.Drawing.Point(6, 4);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidth = 62;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(219, 250);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
			// 
			// Form_ChonDichVu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(244, 472);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Form_ChonDichVu";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form_ChonDichVu";
			this.Load += new System.EventHandler(this.Form_ChonDichVu_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudSoLuong)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button btnXacNhan;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnThemDichVu;
		private System.Windows.Forms.NumericUpDown nudSoLuong;
	}
}