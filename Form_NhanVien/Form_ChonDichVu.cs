﻿using QLKS_ADONET.BS_Layer;
using QLKS_ADONET.Form_QuanLy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_NhanVien
{
	public partial class Form_ChonDichVu : Form
	{
		BL_DichVu BL_DichVu = new BL_DichVu();
		public int maDV;
		public int soLuong;
		public int manv;
		public string maphong;

		public Form_ChonDichVu()
		{
			InitializeComponent();
		}
		void LoadData()
		{
			try
			{
				dataGridView1.DataSource = BL_DichVu.DSDichVu();
				dataGridView1.AutoResizeColumns();
			}
			catch (SqlException)
			{
				MessageBox.Show("Không lấy được nội dung trong table KhachHang. Lỗi rồi!!!");
			}
		}
		private void Form_ChonDichVu_Load(object sender, EventArgs e)
		{
			LoadData();
		}

		private void panel2_Paint(object sender, PaintEventArgs e)
		{
			Graphics gp = e.Graphics;
			Pen pen = new Pen(Color.FromArgb(11, 61, 161));
			pen.Width = 3;
			gp.DrawRectangle(pen, 1, 1, panel2.Width - 4, panel2.Height - 4);
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			int r = dataGridView1.CurrentCell.RowIndex;
			this.maDV = Convert.ToInt32(dataGridView1.Rows[r].Cells[0].Value.ToString());
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			this.soLuong = Convert.ToInt32(nudSoLuong.Value);
		}

		private void btnXacNhan_Click(object sender, EventArgs e)
		{
			// Lấy mã đặt phòng hiện tại
			BL_Phong bldatphong = new BL_Phong();
			DatPhong dp = bldatphong.LayDatPhong(this.maphong);
			// Lấy mã dịch vụ
			int r = dataGridView1.CurrentCell.RowIndex;
			this.maDV = Convert.ToInt32(dataGridView1.Rows[r].Cells[0].Value.ToString());
			// Lấy số lượng
			this.soLuong = Convert.ToInt32(nudSoLuong.Value);
			// Thêm dịch vụ vào phòng hiện tại
			BL_Phong blphong =new BL_Phong();
			blphong.ThemDichVu(dp.MaDatPhong, this.maDV, this.soLuong);
			MessageBox.Show("Thành công!");
			this.Close();
		}

		private void btnThemDichVu_Click(object sender, EventArgs e)
		{
			TaoDichVu form = new TaoDichVu();
			form.ShowDialog();
			LoadData();
		}
	}
}
