﻿namespace QLKS_ADONET.Form_NhanVien
{
	partial class Form_DatPhong
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btn_DangXuat = new System.Windows.Forms.Button();
			this.lblSoNguoiO = new System.Windows.Forms.Label();
			this.lblKHDangThue = new System.Windows.Forms.Label();
			this.btnDatPhong = new System.Windows.Forms.Button();
			this.btnChinhSuaKhachHang = new System.Windows.Forms.Button();
			this.btnThemKhachHang = new System.Windows.Forms.Button();
			this.btnThemDichVu = new System.Windows.Forms.Button();
			this.btnTraPhong = new System.Windows.Forms.Button();
			this.btnDonDepXong = new System.Windows.Forms.Button();
			this.lblMaPhong = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lblText = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.lblUsername = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panelPhong = new System.Windows.Forms.Panel();
			this.btnV203_3 = new System.Windows.Forms.Button();
			this.picV203_3 = new System.Windows.Forms.PictureBox();
			this.btnV202_2 = new System.Windows.Forms.Button();
			this.picV202_2 = new System.Windows.Forms.PictureBox();
			this.btnV201_1 = new System.Windows.Forms.Button();
			this.picV201_1 = new System.Windows.Forms.PictureBox();
			this.btnT109_3 = new System.Windows.Forms.Button();
			this.picT109_3 = new System.Windows.Forms.PictureBox();
			this.btnT108_2 = new System.Windows.Forms.Button();
			this.picT108_2 = new System.Windows.Forms.PictureBox();
			this.btnT107_1 = new System.Windows.Forms.Button();
			this.picT107_1 = new System.Windows.Forms.PictureBox();
			this.btnT106_3 = new System.Windows.Forms.Button();
			this.picT106_3 = new System.Windows.Forms.PictureBox();
			this.btnT105_2 = new System.Windows.Forms.Button();
			this.picT105_2 = new System.Windows.Forms.PictureBox();
			this.btnT104_1 = new System.Windows.Forms.Button();
			this.picT104_1 = new System.Windows.Forms.PictureBox();
			this.btnT103_3 = new System.Windows.Forms.Button();
			this.T103_3 = new System.Windows.Forms.PictureBox();
			this.btnT102_2 = new System.Windows.Forms.Button();
			this.picT102_2 = new System.Windows.Forms.PictureBox();
			this.btnT101_1 = new System.Windows.Forms.Button();
			this.picT101_1 = new System.Windows.Forms.PictureBox();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.panelPhong.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picV203_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picV202_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picV201_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT109_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT108_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT107_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT106_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT105_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT104_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T103_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT102_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picT101_1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.btn_DangXuat);
			this.panel1.Controls.Add(this.lblSoNguoiO);
			this.panel1.Controls.Add(this.lblKHDangThue);
			this.panel1.Controls.Add(this.btnDatPhong);
			this.panel1.Controls.Add(this.btnChinhSuaKhachHang);
			this.panel1.Controls.Add(this.btnThemKhachHang);
			this.panel1.Controls.Add(this.btnThemDichVu);
			this.panel1.Controls.Add(this.btnTraPhong);
			this.panel1.Controls.Add(this.btnDonDepXong);
			this.panel1.Controls.Add(this.lblMaPhong);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.ForeColor = System.Drawing.Color.Black;
			this.panel1.Location = new System.Drawing.Point(0, 98);
			this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(278, 876);
			this.panel1.TabIndex = 0;
			// 
			// btn_DangXuat
			// 
			this.btn_DangXuat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(205)))), ((int)(((byte)(217)))));
			this.btn_DangXuat.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_DangXuat.FlatAppearance.BorderSize = 0;
			this.btn_DangXuat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_DangXuat.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_DangXuat.ForeColor = System.Drawing.Color.Black;
			this.btn_DangXuat.Image = global::QLKS_ADONET.Properties.Resources.logout;
			this.btn_DangXuat.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btn_DangXuat.Location = new System.Drawing.Point(15, 592);
			this.btn_DangXuat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_DangXuat.Name = "btn_DangXuat";
			this.btn_DangXuat.Size = new System.Drawing.Size(246, 66);
			this.btn_DangXuat.TabIndex = 14;
			this.btn_DangXuat.Text = "Đăng xuất   ";
			this.btn_DangXuat.UseVisualStyleBackColor = false;
			this.btn_DangXuat.Click += new System.EventHandler(this.btn_DangXuat_Click);
			// 
			// lblSoNguoiO
			// 
			this.lblSoNguoiO.AutoSize = true;
			this.lblSoNguoiO.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSoNguoiO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.lblSoNguoiO.Location = new System.Drawing.Point(15, 802);
			this.lblSoNguoiO.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSoNguoiO.Name = "lblSoNguoiO";
			this.lblSoNguoiO.Size = new System.Drawing.Size(124, 27);
			this.lblSoNguoiO.TabIndex = 13;
			this.lblSoNguoiO.Text = "Số người ở";
			// 
			// lblKHDangThue
			// 
			this.lblKHDangThue.AutoSize = true;
			this.lblKHDangThue.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblKHDangThue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.lblKHDangThue.Location = new System.Drawing.Point(14, 757);
			this.lblKHDangThue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKHDangThue.Name = "lblKHDangThue";
			this.lblKHDangThue.Size = new System.Drawing.Size(183, 27);
			this.lblKHDangThue.TabIndex = 12;
			this.lblKHDangThue.Text = "Tên Khách hàng";
			// 
			// btnDatPhong
			// 
			this.btnDatPhong.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnDatPhong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnDatPhong.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnDatPhong.FlatAppearance.BorderSize = 0;
			this.btnDatPhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDatPhong.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDatPhong.ForeColor = System.Drawing.Color.White;
			this.btnDatPhong.Location = new System.Drawing.Point(15, 381);
			this.btnDatPhong.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnDatPhong.Name = "btnDatPhong";
			this.btnDatPhong.Size = new System.Drawing.Size(246, 66);
			this.btnDatPhong.TabIndex = 11;
			this.btnDatPhong.Text = "Đặt Phòng";
			this.btnDatPhong.UseVisualStyleBackColor = false;
			this.btnDatPhong.Click += new System.EventHandler(this.btnDatPhong_Click);
			// 
			// btnChinhSuaKhachHang
			// 
			this.btnChinhSuaKhachHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(205)))), ((int)(((byte)(217)))));
			this.btnChinhSuaKhachHang.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChinhSuaKhachHang.FlatAppearance.BorderSize = 0;
			this.btnChinhSuaKhachHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSuaKhachHang.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSuaKhachHang.ForeColor = System.Drawing.Color.Black;
			this.btnChinhSuaKhachHang.Location = new System.Drawing.Point(15, 506);
			this.btnChinhSuaKhachHang.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnChinhSuaKhachHang.Name = "btnChinhSuaKhachHang";
			this.btnChinhSuaKhachHang.Size = new System.Drawing.Size(246, 66);
			this.btnChinhSuaKhachHang.TabIndex = 10;
			this.btnChinhSuaKhachHang.Text = "Thay đổi KH";
			this.btnChinhSuaKhachHang.UseVisualStyleBackColor = false;
			this.btnChinhSuaKhachHang.Click += new System.EventHandler(this.btnChinhSuaKhachHang_Click);
			// 
			// btnThemKhachHang
			// 
			this.btnThemKhachHang.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnThemKhachHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnThemKhachHang.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnThemKhachHang.FlatAppearance.BorderSize = 0;
			this.btnThemKhachHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThemKhachHang.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThemKhachHang.ForeColor = System.Drawing.Color.White;
			this.btnThemKhachHang.Location = new System.Drawing.Point(15, 293);
			this.btnThemKhachHang.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnThemKhachHang.Name = "btnThemKhachHang";
			this.btnThemKhachHang.Size = new System.Drawing.Size(246, 66);
			this.btnThemKhachHang.TabIndex = 2;
			this.btnThemKhachHang.Text = "Thêm KH";
			this.btnThemKhachHang.UseVisualStyleBackColor = false;
			this.btnThemKhachHang.Click += new System.EventHandler(this.btnThemKhachHang_Click);
			// 
			// btnThemDichVu
			// 
			this.btnThemDichVu.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnThemDichVu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnThemDichVu.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnThemDichVu.FlatAppearance.BorderSize = 0;
			this.btnThemDichVu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThemDichVu.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThemDichVu.ForeColor = System.Drawing.Color.White;
			this.btnThemDichVu.Location = new System.Drawing.Point(15, 206);
			this.btnThemDichVu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnThemDichVu.Name = "btnThemDichVu";
			this.btnThemDichVu.Size = new System.Drawing.Size(246, 66);
			this.btnThemDichVu.TabIndex = 3;
			this.btnThemDichVu.Text = "Thêm Dịch Vụ";
			this.btnThemDichVu.UseVisualStyleBackColor = false;
			this.btnThemDichVu.Click += new System.EventHandler(this.btnThemDichVu_Click);
			// 
			// btnTraPhong
			// 
			this.btnTraPhong.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnTraPhong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnTraPhong.FlatAppearance.BorderSize = 0;
			this.btnTraPhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTraPhong.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTraPhong.ForeColor = System.Drawing.Color.White;
			this.btnTraPhong.Location = new System.Drawing.Point(15, 121);
			this.btnTraPhong.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnTraPhong.Name = "btnTraPhong";
			this.btnTraPhong.Size = new System.Drawing.Size(246, 66);
			this.btnTraPhong.TabIndex = 4;
			this.btnTraPhong.Text = "Trả Phòng";
			this.btnTraPhong.UseVisualStyleBackColor = false;
			this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
			// 
			// btnDonDepXong
			// 
			this.btnDonDepXong.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnDonDepXong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnDonDepXong.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnDonDepXong.FlatAppearance.BorderSize = 0;
			this.btnDonDepXong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDonDepXong.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDonDepXong.ForeColor = System.Drawing.Color.White;
			this.btnDonDepXong.Location = new System.Drawing.Point(15, 35);
			this.btnDonDepXong.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnDonDepXong.Name = "btnDonDepXong";
			this.btnDonDepXong.Size = new System.Drawing.Size(246, 66);
			this.btnDonDepXong.TabIndex = 3;
			this.btnDonDepXong.Text = "Dọn Dẹp Xong";
			this.btnDonDepXong.UseVisualStyleBackColor = false;
			this.btnDonDepXong.Click += new System.EventHandler(this.btnDonDepXong_Click);
			// 
			// lblMaPhong
			// 
			this.lblMaPhong.AutoSize = true;
			this.lblMaPhong.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMaPhong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.lblMaPhong.Location = new System.Drawing.Point(14, 708);
			this.lblMaPhong.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMaPhong.Name = "lblMaPhong";
			this.lblMaPhong.Size = new System.Drawing.Size(241, 27);
			this.lblMaPhong.TabIndex = 9;
			this.lblMaPhong.Text = "Mã Phòng Đang chọn";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.panel2.Controls.Add(this.lblText);
			this.panel2.Controls.Add(this.pictureBox2);
			this.panel2.Controls.Add(this.lblUsername);
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1426, 98);
			this.panel2.TabIndex = 0;
			// 
			// lblText
			// 
			this.lblText.AutoSize = true;
			this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(161)))), ((int)(((byte)(178)))));
			this.lblText.Location = new System.Drawing.Point(116, 55);
			this.lblText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblText.Name = "lblText";
			this.lblText.Size = new System.Drawing.Size(179, 20);
			this.lblText.TabIndex = 2;
			this.lblText.Text = "Some user text here";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox2.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox2.Location = new System.Drawing.Point(1353, 29);
			this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(38, 38);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 1;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// lblUsername
			// 
			this.lblUsername.AutoSize = true;
			this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
			this.lblUsername.ForeColor = System.Drawing.Color.White;
			this.lblUsername.Location = new System.Drawing.Point(116, 23);
			this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(119, 25);
			this.lblUsername.TabIndex = 1;
			this.lblUsername.Text = "User Name";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::QLKS_ADONET.Properties.Resources.man;
			this.pictureBox1.Location = new System.Drawing.Point(18, 11);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(76, 77);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// panelPhong
			// 
			this.panelPhong.BackColor = System.Drawing.Color.White;
			this.panelPhong.Controls.Add(this.btnV203_3);
			this.panelPhong.Controls.Add(this.picV203_3);
			this.panelPhong.Controls.Add(this.btnV202_2);
			this.panelPhong.Controls.Add(this.picV202_2);
			this.panelPhong.Controls.Add(this.btnV201_1);
			this.panelPhong.Controls.Add(this.picV201_1);
			this.panelPhong.Controls.Add(this.btnT109_3);
			this.panelPhong.Controls.Add(this.picT109_3);
			this.panelPhong.Controls.Add(this.btnT108_2);
			this.panelPhong.Controls.Add(this.picT108_2);
			this.panelPhong.Controls.Add(this.btnT107_1);
			this.panelPhong.Controls.Add(this.picT107_1);
			this.panelPhong.Controls.Add(this.btnT106_3);
			this.panelPhong.Controls.Add(this.picT106_3);
			this.panelPhong.Controls.Add(this.btnT105_2);
			this.panelPhong.Controls.Add(this.picT105_2);
			this.panelPhong.Controls.Add(this.btnT104_1);
			this.panelPhong.Controls.Add(this.picT104_1);
			this.panelPhong.Controls.Add(this.btnT103_3);
			this.panelPhong.Controls.Add(this.T103_3);
			this.panelPhong.Controls.Add(this.btnT102_2);
			this.panelPhong.Controls.Add(this.picT102_2);
			this.panelPhong.Controls.Add(this.btnT101_1);
			this.panelPhong.Controls.Add(this.picT101_1);
			this.panelPhong.Location = new System.Drawing.Point(288, 108);
			this.panelPhong.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panelPhong.Name = "panelPhong";
			this.panelPhong.Size = new System.Drawing.Size(1128, 854);
			this.panelPhong.TabIndex = 2;
			// 
			// btnV203_3
			// 
			this.btnV203_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnV203_3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnV203_3.FlatAppearance.BorderSize = 0;
			this.btnV203_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnV203_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnV203_3.ForeColor = System.Drawing.Color.White;
			this.btnV203_3.Location = new System.Drawing.Point(873, 752);
			this.btnV203_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnV203_3.Name = "btnV203_3";
			this.btnV203_3.Size = new System.Drawing.Size(212, 58);
			this.btnV203_3.TabIndex = 23;
			this.btnV203_3.Text = "V203 3";
			this.btnV203_3.UseVisualStyleBackColor = false;
			this.btnV203_3.Click += new System.EventHandler(this.btnV203_3_Click);
			// 
			// picV203_3
			// 
			this.picV203_3.Image = global::QLKS_ADONET.Properties.Resources.roomsvip;
			this.picV203_3.Location = new System.Drawing.Point(873, 583);
			this.picV203_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picV203_3.Name = "picV203_3";
			this.picV203_3.Size = new System.Drawing.Size(212, 154);
			this.picV203_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picV203_3.TabIndex = 22;
			this.picV203_3.TabStop = false;
			// 
			// btnV202_2
			// 
			this.btnV202_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnV202_2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnV202_2.FlatAppearance.BorderSize = 0;
			this.btnV202_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnV202_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnV202_2.ForeColor = System.Drawing.Color.White;
			this.btnV202_2.Location = new System.Drawing.Point(590, 752);
			this.btnV202_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnV202_2.Name = "btnV202_2";
			this.btnV202_2.Size = new System.Drawing.Size(212, 58);
			this.btnV202_2.TabIndex = 21;
			this.btnV202_2.Text = "V202 2";
			this.btnV202_2.UseVisualStyleBackColor = false;
			this.btnV202_2.Click += new System.EventHandler(this.btnV202_2_Click);
			// 
			// picV202_2
			// 
			this.picV202_2.Image = global::QLKS_ADONET.Properties.Resources.roomsvip;
			this.picV202_2.Location = new System.Drawing.Point(590, 583);
			this.picV202_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picV202_2.Name = "picV202_2";
			this.picV202_2.Size = new System.Drawing.Size(212, 154);
			this.picV202_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picV202_2.TabIndex = 20;
			this.picV202_2.TabStop = false;
			// 
			// btnV201_1
			// 
			this.btnV201_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
			this.btnV201_1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnV201_1.FlatAppearance.BorderSize = 0;
			this.btnV201_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnV201_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnV201_1.ForeColor = System.Drawing.Color.White;
			this.btnV201_1.Location = new System.Drawing.Point(312, 752);
			this.btnV201_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnV201_1.Name = "btnV201_1";
			this.btnV201_1.Size = new System.Drawing.Size(212, 58);
			this.btnV201_1.TabIndex = 19;
			this.btnV201_1.Text = "V201 1";
			this.btnV201_1.UseVisualStyleBackColor = false;
			this.btnV201_1.Click += new System.EventHandler(this.btnV201_1_Click);
			// 
			// picV201_1
			// 
			this.picV201_1.Image = global::QLKS_ADONET.Properties.Resources.roomsvip;
			this.picV201_1.Location = new System.Drawing.Point(312, 583);
			this.picV201_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picV201_1.Name = "picV201_1";
			this.picV201_1.Size = new System.Drawing.Size(212, 154);
			this.picV201_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picV201_1.TabIndex = 18;
			this.picV201_1.TabStop = false;
			// 
			// btnT109_3
			// 
			this.btnT109_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnT109_3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT109_3.FlatAppearance.BorderSize = 0;
			this.btnT109_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT109_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT109_3.ForeColor = System.Drawing.Color.White;
			this.btnT109_3.Location = new System.Drawing.Point(26, 752);
			this.btnT109_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT109_3.Name = "btnT109_3";
			this.btnT109_3.Size = new System.Drawing.Size(212, 58);
			this.btnT109_3.TabIndex = 17;
			this.btnT109_3.Text = "T109 3";
			this.btnT109_3.UseVisualStyleBackColor = false;
			this.btnT109_3.Click += new System.EventHandler(this.btnT109_3_Click);
			// 
			// picT109_3
			// 
			this.picT109_3.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT109_3.Location = new System.Drawing.Point(26, 575);
			this.picT109_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT109_3.Name = "picT109_3";
			this.picT109_3.Size = new System.Drawing.Size(212, 154);
			this.picT109_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT109_3.TabIndex = 16;
			this.picT109_3.TabStop = false;
			// 
			// btnT108_2
			// 
			this.btnT108_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnT108_2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT108_2.FlatAppearance.BorderSize = 0;
			this.btnT108_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT108_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT108_2.ForeColor = System.Drawing.Color.White;
			this.btnT108_2.Location = new System.Drawing.Point(873, 477);
			this.btnT108_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT108_2.Name = "btnT108_2";
			this.btnT108_2.Size = new System.Drawing.Size(212, 58);
			this.btnT108_2.TabIndex = 15;
			this.btnT108_2.Text = "T108 2";
			this.btnT108_2.UseVisualStyleBackColor = false;
			this.btnT108_2.Click += new System.EventHandler(this.btnT108_2_Click);
			// 
			// picT108_2
			// 
			this.picT108_2.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT108_2.Location = new System.Drawing.Point(873, 300);
			this.picT108_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT108_2.Name = "picT108_2";
			this.picT108_2.Size = new System.Drawing.Size(212, 154);
			this.picT108_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT108_2.TabIndex = 14;
			this.picT108_2.TabStop = false;
			// 
			// btnT107_1
			// 
			this.btnT107_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
			this.btnT107_1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT107_1.FlatAppearance.BorderSize = 0;
			this.btnT107_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT107_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT107_1.ForeColor = System.Drawing.Color.White;
			this.btnT107_1.Location = new System.Drawing.Point(590, 477);
			this.btnT107_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT107_1.Name = "btnT107_1";
			this.btnT107_1.Size = new System.Drawing.Size(212, 58);
			this.btnT107_1.TabIndex = 13;
			this.btnT107_1.Text = "T107 1";
			this.btnT107_1.UseVisualStyleBackColor = false;
			this.btnT107_1.Click += new System.EventHandler(this.btnT107_1_Click);
			// 
			// picT107_1
			// 
			this.picT107_1.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT107_1.Location = new System.Drawing.Point(590, 300);
			this.picT107_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT107_1.Name = "picT107_1";
			this.picT107_1.Size = new System.Drawing.Size(212, 154);
			this.picT107_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT107_1.TabIndex = 12;
			this.picT107_1.TabStop = false;
			// 
			// btnT106_3
			// 
			this.btnT106_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
			this.btnT106_3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT106_3.FlatAppearance.BorderSize = 0;
			this.btnT106_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT106_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT106_3.ForeColor = System.Drawing.Color.White;
			this.btnT106_3.Location = new System.Drawing.Point(312, 477);
			this.btnT106_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT106_3.Name = "btnT106_3";
			this.btnT106_3.Size = new System.Drawing.Size(212, 58);
			this.btnT106_3.TabIndex = 11;
			this.btnT106_3.Text = "T106 3";
			this.btnT106_3.UseVisualStyleBackColor = false;
			this.btnT106_3.Click += new System.EventHandler(this.btnT106_3_Click);
			// 
			// picT106_3
			// 
			this.picT106_3.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT106_3.Location = new System.Drawing.Point(312, 300);
			this.picT106_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT106_3.Name = "picT106_3";
			this.picT106_3.Size = new System.Drawing.Size(212, 154);
			this.picT106_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT106_3.TabIndex = 10;
			this.picT106_3.TabStop = false;
			// 
			// btnT105_2
			// 
			this.btnT105_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
			this.btnT105_2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT105_2.FlatAppearance.BorderSize = 0;
			this.btnT105_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT105_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT105_2.ForeColor = System.Drawing.Color.White;
			this.btnT105_2.Location = new System.Drawing.Point(26, 477);
			this.btnT105_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT105_2.Name = "btnT105_2";
			this.btnT105_2.Size = new System.Drawing.Size(212, 58);
			this.btnT105_2.TabIndex = 9;
			this.btnT105_2.Text = "T105 2";
			this.btnT105_2.UseVisualStyleBackColor = false;
			this.btnT105_2.Click += new System.EventHandler(this.btnT105_2_Click);
			// 
			// picT105_2
			// 
			this.picT105_2.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT105_2.Location = new System.Drawing.Point(26, 300);
			this.picT105_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT105_2.Name = "picT105_2";
			this.picT105_2.Size = new System.Drawing.Size(212, 154);
			this.picT105_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT105_2.TabIndex = 8;
			this.picT105_2.TabStop = false;
			// 
			// btnT104_1
			// 
			this.btnT104_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnT104_1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT104_1.FlatAppearance.BorderSize = 0;
			this.btnT104_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT104_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT104_1.ForeColor = System.Drawing.Color.White;
			this.btnT104_1.Location = new System.Drawing.Point(873, 206);
			this.btnT104_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT104_1.Name = "btnT104_1";
			this.btnT104_1.Size = new System.Drawing.Size(212, 58);
			this.btnT104_1.TabIndex = 7;
			this.btnT104_1.Text = "T104 1";
			this.btnT104_1.UseVisualStyleBackColor = false;
			this.btnT104_1.Click += new System.EventHandler(this.btnT104_1_Click);
			// 
			// picT104_1
			// 
			this.picT104_1.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT104_1.Location = new System.Drawing.Point(873, 29);
			this.picT104_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT104_1.Name = "picT104_1";
			this.picT104_1.Size = new System.Drawing.Size(212, 154);
			this.picT104_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT104_1.TabIndex = 6;
			this.picT104_1.TabStop = false;
			// 
			// btnT103_3
			// 
			this.btnT103_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnT103_3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT103_3.FlatAppearance.BorderSize = 0;
			this.btnT103_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT103_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT103_3.ForeColor = System.Drawing.Color.White;
			this.btnT103_3.Location = new System.Drawing.Point(590, 206);
			this.btnT103_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT103_3.Name = "btnT103_3";
			this.btnT103_3.Size = new System.Drawing.Size(212, 58);
			this.btnT103_3.TabIndex = 5;
			this.btnT103_3.Text = "T103 3";
			this.btnT103_3.UseVisualStyleBackColor = false;
			this.btnT103_3.Click += new System.EventHandler(this.button3_Click);
			// 
			// T103_3
			// 
			this.T103_3.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.T103_3.Location = new System.Drawing.Point(590, 29);
			this.T103_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.T103_3.Name = "T103_3";
			this.T103_3.Size = new System.Drawing.Size(212, 154);
			this.T103_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.T103_3.TabIndex = 4;
			this.T103_3.TabStop = false;
			// 
			// btnT102_2
			// 
			this.btnT102_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
			this.btnT102_2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT102_2.FlatAppearance.BorderSize = 0;
			this.btnT102_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT102_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT102_2.ForeColor = System.Drawing.Color.White;
			this.btnT102_2.Location = new System.Drawing.Point(312, 206);
			this.btnT102_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT102_2.Name = "btnT102_2";
			this.btnT102_2.Size = new System.Drawing.Size(212, 58);
			this.btnT102_2.TabIndex = 3;
			this.btnT102_2.Text = "T102 2";
			this.btnT102_2.UseVisualStyleBackColor = false;
			this.btnT102_2.Click += new System.EventHandler(this.btnT102_2_Click);
			// 
			// picT102_2
			// 
			this.picT102_2.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT102_2.Location = new System.Drawing.Point(312, 29);
			this.picT102_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT102_2.Name = "picT102_2";
			this.picT102_2.Size = new System.Drawing.Size(212, 154);
			this.picT102_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT102_2.TabIndex = 2;
			this.picT102_2.TabStop = false;
			// 
			// btnT101_1
			// 
			this.btnT101_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
			this.btnT101_1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnT101_1.FlatAppearance.BorderSize = 0;
			this.btnT101_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnT101_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnT101_1.ForeColor = System.Drawing.Color.White;
			this.btnT101_1.Location = new System.Drawing.Point(26, 206);
			this.btnT101_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnT101_1.Name = "btnT101_1";
			this.btnT101_1.Size = new System.Drawing.Size(212, 58);
			this.btnT101_1.TabIndex = 1;
			this.btnT101_1.Text = "T101 1";
			this.btnT101_1.UseVisualStyleBackColor = false;
			this.btnT101_1.Click += new System.EventHandler(this.btnT101_1_Click);
			// 
			// picT101_1
			// 
			this.picT101_1.Image = global::QLKS_ADONET.Properties.Resources.rooms;
			this.picT101_1.Location = new System.Drawing.Point(26, 29);
			this.picT101_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picT101_1.Name = "picT101_1";
			this.picT101_1.Size = new System.Drawing.Size(212, 154);
			this.picT101_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picT101_1.TabIndex = 0;
			this.picT101_1.TabStop = false;
			// 
			// Form_DatPhong
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(1426, 974);
			this.Controls.Add(this.panelPhong);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "Form_DatPhong";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form_DatPhong";
			this.Load += new System.EventHandler(this.Form_DatPhong_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.panelPhong.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.picV203_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picV202_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picV201_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT109_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT108_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT107_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT106_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT105_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT104_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T103_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT102_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picT101_1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.Panel panelPhong;
		private System.Windows.Forms.PictureBox picT101_1;
		private System.Windows.Forms.Button btnT101_1;
		private System.Windows.Forms.Button btnT103_3;
		private System.Windows.Forms.PictureBox T103_3;
		private System.Windows.Forms.Button btnT102_2;
		private System.Windows.Forms.PictureBox picT102_2;
		private System.Windows.Forms.Button btnV203_3;
		private System.Windows.Forms.PictureBox picV203_3;
		private System.Windows.Forms.Button btnV202_2;
		private System.Windows.Forms.PictureBox picV202_2;
		private System.Windows.Forms.Button btnV201_1;
		private System.Windows.Forms.PictureBox picV201_1;
		private System.Windows.Forms.Button btnT109_3;
		private System.Windows.Forms.PictureBox picT109_3;
		private System.Windows.Forms.Button btnT108_2;
		private System.Windows.Forms.PictureBox picT108_2;
		private System.Windows.Forms.Button btnT107_1;
		private System.Windows.Forms.PictureBox picT107_1;
		private System.Windows.Forms.Button btnT106_3;
		private System.Windows.Forms.PictureBox picT106_3;
		private System.Windows.Forms.Button btnT105_2;
		private System.Windows.Forms.PictureBox picT105_2;
		private System.Windows.Forms.Button btnT104_1;
		private System.Windows.Forms.PictureBox picT104_1;
		private System.Windows.Forms.Button btnThemKhachHang;
		private System.Windows.Forms.Button btnTraPhong;
		private System.Windows.Forms.Button btnThemDichVu;
		private System.Windows.Forms.Button btnDonDepXong;
		private System.Windows.Forms.Label lblMaPhong;
		private System.Windows.Forms.Button btnChinhSuaKhachHang;
		private System.Windows.Forms.Button btnDatPhong;
		private System.Windows.Forms.Label lblKHDangThue;
		private System.Windows.Forms.Label lblSoNguoiO;
		private System.Windows.Forms.Label lblText;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button btn_DangXuat;
	}
}