﻿using QLKS_ADONET.BS_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_NhanVien
{
	public partial class Form_DatPhong : Form
	{
		Dictionary<int, string> myColor = new Dictionary<int, string>();
		MyPhong myPhong;
		DatPhong datPhong;
		public NhanVien nhanVien;
		public KhachHang khachHang;
		public int soNguoiO;
		public float tienCoc;
		public int soNgayO;

		public Form_DatPhong()
		{
			InitializeComponent();
			myColor.Add(3, "237, 57, 57");
			myColor.Add(2, "156, 148, 148");
			myColor.Add(1, "36, 171, 41");
			DonPhong(false);
			PhongSanSang(false);
			PhongDangThue(false);
		}
		private void DonPhong(bool flag)
		{
			if (flag)
			{
				btnDonDepXong.Enabled = true;
			}
			else
			{
				btnDonDepXong.Enabled = false;
			}
		}
		private void PhongSanSang(bool flag)
		{
			if (flag)
			{
				btnDatPhong.Enabled = true;
				btnThemKhachHang.Enabled = true;
			}
			else
			{
				btnDatPhong.Enabled = false;
				btnThemKhachHang.Enabled = false;
			}

		}
		private void PhongDangThue(bool flag)
		{
			if (flag)
			{
				btnTraPhong.Enabled = true;
				btnThemDichVu.Enabled = true;
			}
			else
			{
				btnTraPhong.Enabled = false;
				btnThemDichVu.Enabled = false;
			}

		}
		private void pictureBox2_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T103");
		}

		private void Form_DatPhong_Load(object sender, EventArgs e)
		{
			LoadData();
			lblUsername.Text = this.nhanVien.HoTen;
			lblText.Text = this.nhanVien.Email;

		}
		private void LoadData()
		{
			BL_Phong blphong = new BL_Phong();
			List<MyPhong> myPhongs = blphong.LayDSPhong();
			foreach (var x in myPhongs)
			{
				string color = myColor[x.TinhTrang];
				string[] parts = color.Split(',');
				int red = int.Parse(parts[0]);
				int green = int.Parse(parts[1]);
				int blue = int.Parse(parts[2]);
				string MyPhong = "btn" + x.MaPhong.ToUpper() + "_" + x.SucChua.ToString();
				Control[] controls = panelPhong.Controls.Find(MyPhong, true);
				Button myButton = controls[0] as Button;
				myButton.BackColor = Color.FromArgb(red, green, blue);
			}
			DonPhong(false);
			PhongDangThue(false);
			PhongSanSang(false);
			myPhong = null;

		}
		private void btnT101_1_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T101");
		}
		private void LayPhongVaHienPanel(string MaPhong)
		{
			DonPhong(false);
			PhongDangThue(false);
			PhongSanSang(false);

			BL_Phong blphong = new BL_Phong();
			List<MyPhong> myPhongs = blphong.LayDSPhong();
			myPhong = myPhongs.Find(m => m.MaPhong == MaPhong);
			// Tìm phòng đang click chuột
			lblMaPhong.Text = "Phòng hiện tại: " + MaPhong;

			// Tìm đặt phòng hiện tại;
			datPhong = blphong.LayDatPhong(MaPhong);
			if (datPhong != null)
			{
				// Tìm khách hàng đang ở phòng đó
				BL_KhachHang blkhachhang = new BL_KhachHang();
				this.khachHang = blkhachhang.TimKiemKhachHang_MaKH(datPhong.MaKH);
				lblKHDangThue.Text = this.khachHang.HoTen;
				lblSoNguoiO.Text = "Số người ở: " + datPhong.SoNguoiO.ToString();
			}
			else
			{
				lblKHDangThue.Text = "Tên khách hàng";
				lblSoNguoiO.Text = "Số người ở";
			}

			if (myPhong.TinhTrang == 1)
				PhongSanSang(true);
			else if (myPhong.TinhTrang == 2)
				PhongDangThue(true);
			else
				DonPhong(true);
		}

		private void btnT102_2_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T102");
		}

		private void btnT104_1_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T104");
		}

		private void btnT105_2_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T105");
		}

		private void btnT106_3_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T106");
		}

		private void btnT107_1_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T107");
		}

		private void btnT108_2_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T108");
		}

		private void btnT109_3_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("T109");
		}

		private void btnV201_1_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("V201");
		}

		private void btnV202_2_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("V202");
		}

		private void btnV203_3_Click(object sender, EventArgs e)
		{
			LayPhongVaHienPanel("V203");
		}

		private void btnDonDepXong_Click(object sender, EventArgs e)
		{
			if (myPhong != null)
			{
				BL_Phong blphong = new BL_Phong();
				blphong.DonPhong(myPhong.MaPhong);
				MessageBox.Show("Đã dọn dẹp Phòng xong", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				LoadData();
			}
			else
			{
				MessageBox.Show("Có lỗi xảy ra", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void btnTraPhong_Click(object sender, EventArgs e)
		{

			if (myPhong != null)
			{
				BL_Phong blphong = new BL_Phong();
				blphong.TraPhong(myPhong.MaPhong);
				MessageBox.Show("Đã Trả Phòng Thành Công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				LoadData();
				// Lập hóa đơn
				HoaDon hd = blphong.XuatHoaDon(this.datPhong.MaDatPhong, this.khachHang.MaKH, this.nhanVien.MaNV);
				// Lấy danh sách DV đã dùng
				BL_DichVu bldichvu = new BL_DichVu();
				List<ChiTietDichVu> dsdv = bldichvu.ListDichVu(this.datPhong.MaDatPhong);
				// thêm các thông tin cho form Hóa đơn
				Form_HoaDon form = new Form_HoaDon();
				form.nhanVien = this.nhanVien;
				form.khachHang = this.khachHang;
				form.hoaDon = hd;
				form.maDatPhong = this.datPhong.MaDatPhong;
				form.listDichVu = dsdv;
				form.ShowDialog();
			}
			else
			{
				MessageBox.Show("Có lỗi xảy ra", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		private void btnThemDichVu_Click(object sender, EventArgs e)
		{
			Form_ChonDichVu form = new Form_ChonDichVu();
			form.manv = this.nhanVien.MaNV;
			form.maphong = this.myPhong.MaPhong;
			form.ShowDialog();
		}

		private void btnThemKhachHang_Click(object sender, EventArgs e)
		{
			Form_ThemKhachHang form = new Form_ThemKhachHang();
			form.ValuePassed += SetValue;
			form.ShowDialog();
		}

		private void btnDatPhong_Click(object sender, EventArgs e)
		{
			if (khachHang != null)
			{
				BL_Phong blphong = new BL_Phong();
				blphong.DatPhong(khachHang.MaKH, myPhong.MaPhong, DateTime.UtcNow.Date, DateTime.UtcNow.Date, soNguoiO, tienCoc, nhanVien.MaNV, soNgayO);
				MessageBox.Show("Thành công");
				LoadData();
				khachHang = null;
				soNguoiO = 0;
				tienCoc = 0;
			}
			else
			{
				MessageBox.Show("Không tìm thấy khách hàng");
			}
		}

		private void btnChinhSuaKhachHang_Click(object sender, EventArgs e)
		{
			Form form = new Form_ThongTinKhachHang();
			form.ShowDialog();
		}

		public void SetValue(KhachHang kh, int songuoio, float tiencoc, int songayo)
		{
			khachHang = kh;
			soNguoiO = songuoio;
			tienCoc = tiencoc;
			soNgayO = songayo;
		}

		private void btn_DangXuat_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
