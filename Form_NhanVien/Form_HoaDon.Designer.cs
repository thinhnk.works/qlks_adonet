﻿namespace QLKS_ADONET.Form_NhanVien
{
	partial class Form_HoaDon
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel7 = new System.Windows.Forms.Panel();
			this.label48 = new System.Windows.Forms.Label();
			this.label47 = new System.Windows.Forms.Label();
			this.panel6 = new System.Windows.Forms.Panel();
			this.label46 = new System.Windows.Forms.Label();
			this.label45 = new System.Windows.Forms.Label();
			this.panel14 = new System.Windows.Forms.Panel();
			this.label43 = new System.Windows.Forms.Label();
			this.lblGiaTienThanhToan = new System.Windows.Forms.Label();
			this.panel13 = new System.Windows.Forms.Panel();
			this.label41 = new System.Windows.Forms.Label();
			this.lblKhuyenMai = new System.Windows.Forms.Label();
			this.panel12 = new System.Windows.Forms.Panel();
			this.label39 = new System.Windows.Forms.Label();
			this.lblTongTien = new System.Windows.Forms.Label();
			this.panelMatHangSuDung = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.lblEmailNhanVien = new System.Windows.Forms.Label();
			this.lblTenNhanVien = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.lblSDTKhachHang = new System.Windows.Forms.Label();
			this.lblTenKhachHang = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lblDate = new System.Windows.Forms.Label();
			this.lblInvoice = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel7.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel14.SuspendLayout();
			this.panel13.SuspendLayout();
			this.panel12.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panel7);
			this.panel1.Controls.Add(this.panel6);
			this.panel1.Controls.Add(this.panelMatHangSuDung);
			this.panel1.Controls.Add(this.panel4);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(356, 597);
			this.panel1.TabIndex = 0;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.label48);
			this.panel7.Controls.Add(this.label47);
			this.panel7.Location = new System.Drawing.Point(11, 486);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(338, 100);
			this.panel7.TabIndex = 5;
			// 
			// label48
			// 
			this.label48.AutoSize = true;
			this.label48.Location = new System.Drawing.Point(80, 70);
			this.label48.Name = "label48";
			this.label48.Size = new System.Drawing.Size(168, 13);
			this.label48.TabIndex = 1;
			this.label48.Text = "ĐC: Số 1, Võ Văn Ngân, Thủ Đức";
			// 
			// label47
			// 
			this.label47.AutoSize = true;
			this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.label47.Location = new System.Drawing.Point(67, 35);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(186, 20);
			this.label47.TabIndex = 0;
			this.label47.Text = "Cảm ơn và hẹn gặp lại";
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.label46);
			this.panel6.Controls.Add(this.label45);
			this.panel6.Controls.Add(this.panel14);
			this.panel6.Controls.Add(this.panel13);
			this.panel6.Controls.Add(this.panel12);
			this.panel6.Location = new System.Drawing.Point(7, 380);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(342, 100);
			this.panel6.TabIndex = 4;
			// 
			// label46
			// 
			this.label46.AutoSize = true;
			this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.label46.Location = new System.Drawing.Point(41, 51);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(48, 13);
			this.label46.TabIndex = 4;
			this.label46.Text = "Tiền mặt";
			// 
			// label45
			// 
			this.label45.AutoSize = true;
			this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label45.Location = new System.Drawing.Point(6, 27);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(123, 12);
			this.label45.TabIndex = 3;
			this.label45.Text = "Phương thức thanh toán";
			// 
			// panel14
			// 
			this.panel14.Controls.Add(this.label43);
			this.panel14.Controls.Add(this.lblGiaTienThanhToan);
			this.panel14.Location = new System.Drawing.Point(142, 58);
			this.panel14.Name = "panel14";
			this.panel14.Size = new System.Drawing.Size(200, 39);
			this.panel14.TabIndex = 2;
			// 
			// label43
			// 
			this.label43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.label43.Dock = System.Windows.Forms.DockStyle.Right;
			this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label43.ForeColor = System.Drawing.Color.White;
			this.label43.Location = new System.Drawing.Point(0, 0);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(118, 39);
			this.label43.TabIndex = 6;
			this.label43.Text = "Grand Total";
			this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGiaTienThanhToan
			// 
			this.lblGiaTienThanhToan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.lblGiaTienThanhToan.Dock = System.Windows.Forms.DockStyle.Right;
			this.lblGiaTienThanhToan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGiaTienThanhToan.ForeColor = System.Drawing.Color.White;
			this.lblGiaTienThanhToan.Location = new System.Drawing.Point(118, 0);
			this.lblGiaTienThanhToan.Name = "lblGiaTienThanhToan";
			this.lblGiaTienThanhToan.Size = new System.Drawing.Size(82, 39);
			this.lblGiaTienThanhToan.TabIndex = 5;
			this.lblGiaTienThanhToan.Text = "432000$";
			this.lblGiaTienThanhToan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panel13
			// 
			this.panel13.Controls.Add(this.label41);
			this.panel13.Controls.Add(this.lblKhuyenMai);
			this.panel13.Location = new System.Drawing.Point(142, 29);
			this.panel13.Name = "panel13";
			this.panel13.Size = new System.Drawing.Size(200, 26);
			this.panel13.TabIndex = 1;
			// 
			// label41
			// 
			this.label41.BackColor = System.Drawing.Color.WhiteSmoke;
			this.label41.Dock = System.Windows.Forms.DockStyle.Right;
			this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.label41.Location = new System.Drawing.Point(0, 0);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(103, 26);
			this.label41.TabIndex = 6;
			this.label41.Text = "Discount 10%";
			this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKhuyenMai
			// 
			this.lblKhuyenMai.BackColor = System.Drawing.Color.WhiteSmoke;
			this.lblKhuyenMai.Dock = System.Windows.Forms.DockStyle.Right;
			this.lblKhuyenMai.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblKhuyenMai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.lblKhuyenMai.Location = new System.Drawing.Point(103, 0);
			this.lblKhuyenMai.Name = "lblKhuyenMai";
			this.lblKhuyenMai.Size = new System.Drawing.Size(97, 26);
			this.lblKhuyenMai.TabIndex = 5;
			this.lblKhuyenMai.Text = "48000$";
			this.lblKhuyenMai.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panel12
			// 
			this.panel12.Controls.Add(this.label39);
			this.panel12.Controls.Add(this.lblTongTien);
			this.panel12.Location = new System.Drawing.Point(142, 0);
			this.panel12.Name = "panel12";
			this.panel12.Size = new System.Drawing.Size(200, 26);
			this.panel12.TabIndex = 0;
			// 
			// label39
			// 
			this.label39.BackColor = System.Drawing.Color.White;
			this.label39.Dock = System.Windows.Forms.DockStyle.Right;
			this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label39.ForeColor = System.Drawing.Color.Black;
			this.label39.Location = new System.Drawing.Point(0, 0);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(103, 26);
			this.label39.TabIndex = 6;
			this.label39.Text = "SUB TOTAL";
			this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTongTien
			// 
			this.lblTongTien.BackColor = System.Drawing.Color.White;
			this.lblTongTien.Dock = System.Windows.Forms.DockStyle.Right;
			this.lblTongTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTongTien.ForeColor = System.Drawing.Color.Black;
			this.lblTongTien.Location = new System.Drawing.Point(103, 0);
			this.lblTongTien.Name = "lblTongTien";
			this.lblTongTien.Size = new System.Drawing.Size(97, 26);
			this.lblTongTien.TabIndex = 5;
			this.lblTongTien.Text = "480000$";
			this.lblTongTien.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panelMatHangSuDung
			// 
			this.panelMatHangSuDung.Location = new System.Drawing.Point(7, 191);
			this.panelMatHangSuDung.Name = "panelMatHangSuDung";
			this.panelMatHangSuDung.Size = new System.Drawing.Size(342, 183);
			this.panelMatHangSuDung.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.lblEmailNhanVien);
			this.panel4.Controls.Add(this.lblTenNhanVien);
			this.panel4.Controls.Add(this.label13);
			this.panel4.Controls.Add(this.lblSDTKhachHang);
			this.panel4.Controls.Add(this.lblTenKhachHang);
			this.panel4.Controls.Add(this.label8);
			this.panel4.Location = new System.Drawing.Point(7, 111);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(338, 74);
			this.panel4.TabIndex = 2;
			// 
			// lblEmailNhanVien
			// 
			this.lblEmailNhanVien.AutoSize = true;
			this.lblEmailNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEmailNhanVien.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.lblEmailNhanVien.Location = new System.Drawing.Point(219, 46);
			this.lblEmailNhanVien.Name = "lblEmailNhanVien";
			this.lblEmailNhanVien.Size = new System.Drawing.Size(104, 15);
			this.lblEmailNhanVien.TabIndex = 5;
			this.lblEmailNhanVien.Text = "thinh@gmail.com";
			// 
			// lblTenNhanVien
			// 
			this.lblTenNhanVien.AutoSize = true;
			this.lblTenNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTenNhanVien.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.lblTenNhanVien.Location = new System.Drawing.Point(205, 25);
			this.lblTenNhanVien.Name = "lblTenNhanVien";
			this.lblTenNhanVien.Size = new System.Drawing.Size(101, 18);
			this.lblTenNhanVien.TabIndex = 4;
			this.lblTenNhanVien.Text = "Khang Thinh";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.label13.Location = new System.Drawing.Point(224, 8);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(63, 15);
			this.label13.TabIndex = 3;
			this.label13.Text = "Vote From";
			// 
			// lblSDTKhachHang
			// 
			this.lblSDTKhachHang.AutoSize = true;
			this.lblSDTKhachHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSDTKhachHang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.lblSDTKhachHang.Location = new System.Drawing.Point(4, 46);
			this.lblSDTKhachHang.Name = "lblSDTKhachHang";
			this.lblSDTKhachHang.Size = new System.Drawing.Size(77, 15);
			this.lblSDTKhachHang.TabIndex = 2;
			this.lblSDTKhachHang.Text = "0382324474";
			// 
			// lblTenKhachHang
			// 
			this.lblTenKhachHang.AutoSize = true;
			this.lblTenKhachHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTenKhachHang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.lblTenKhachHang.Location = new System.Drawing.Point(3, 25);
			this.lblTenKhachHang.Name = "lblTenKhachHang";
			this.lblTenKhachHang.Size = new System.Drawing.Size(82, 18);
			this.lblTenKhachHang.TabIndex = 1;
			this.lblTenKhachHang.Text = "Thanh Loi";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.label8.Location = new System.Drawing.Point(4, 8);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(37, 15);
			this.label8.TabIndex = 0;
			this.label8.Text = "Bill to";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lblDate);
			this.panel2.Controls.Add(this.lblInvoice);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Location = new System.Drawing.Point(4, 4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(341, 97);
			this.panel2.TabIndex = 0;
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.lblDate.Location = new System.Drawing.Point(274, 56);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(65, 13);
			this.lblDate.TabIndex = 7;
			this.lblDate.Text = "14/08/2023";
			// 
			// lblInvoice
			// 
			this.lblInvoice.AutoSize = true;
			this.lblInvoice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.lblInvoice.Location = new System.Drawing.Point(302, 35);
			this.lblInvoice.Name = "lblInvoice";
			this.lblInvoice.Size = new System.Drawing.Size(38, 13);
			this.lblInvoice.TabIndex = 6;
			this.lblInvoice.Text = "#1234";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
			this.label5.Location = new System.Drawing.Point(220, 53);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(37, 15);
			this.label5.TabIndex = 5;
			this.label5.Text = "Date";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
			this.label4.Location = new System.Drawing.Point(220, 34);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 15);
			this.label4.TabIndex = 4;
			this.label4.Text = "Invoice";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.label3.Location = new System.Drawing.Point(244, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(93, 24);
			this.label3.TabIndex = 3;
			this.label3.Text = "INVOICE";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(148)))));
			this.label2.Location = new System.Drawing.Point(82, 59);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Private limited";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.label1.Location = new System.Drawing.Point(99, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 22);
			this.label1.TabIndex = 1;
			this.label1.Text = "LYT";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::QLKS_ADONET.Properties.Resources.hotels;
			this.pictureBox1.Location = new System.Drawing.Point(3, 3);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(64, 91);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.button1.FlatAppearance.BorderSize = 0;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(256, 622);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(107, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Xuất Phiếu";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form_HoaDon
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(380, 657);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Form_HoaDon";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form_HoaDon";
			this.Load += new System.EventHandler(this.Form_HoaDon_Load);
			this.panel1.ResumeLayout(false);
			this.panel7.ResumeLayout(false);
			this.panel7.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.panel14.ResumeLayout(false);
			this.panel13.ResumeLayout(false);
			this.panel12.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panelMatHangSuDung;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Label lblInvoice;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblSDTKhachHang;
		private System.Windows.Forms.Label lblTenKhachHang;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label lblEmailNhanVien;
		private System.Windows.Forms.Label lblTenNhanVien;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label lblGiaTienThanhToan;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label lblKhuyenMai;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label lblTongTien;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Button button1;
	}
}