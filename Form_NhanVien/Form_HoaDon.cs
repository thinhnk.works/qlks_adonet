﻿using QLKS_ADONET.BS_Layer;
using QLKS_ADONET.Form_QuanLy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_NhanVien
{

	public partial class Form_HoaDon : Form
	{
		public List<ChiTietDichVu> listDichVu;
		public NhanVien nhanVien;
		public KhachHang khachHang;
		public HoaDon hoaDon;
		public int maDatPhong;

		public Form_HoaDon()
		{
			InitializeComponent();

		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			Graphics gp = e.Graphics;
			Pen pen = new Pen(Color.FromArgb(11, 61, 161));
			pen.Width = 3;
			gp.DrawRectangle(pen, 1, 1, panel1.Width - 4, panel1.Height - 4);
		}

		private void Form_HoaDon_Load(object sender, EventArgs e)
		{
			this.lblDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
			int hoadon = this.hoaDon.MaHoaDon;
			if (hoadon >= 0 && hoadon <= 9)
				this.lblInvoice.Text = "#00" + hoaDon.MaHoaDon.ToString();
			else if (hoadon >= 10 && hoadon <= 99)
				this.lblInvoice.Text = "#0" + hoaDon.MaHoaDon.ToString();
			else
				this.lblInvoice.Text = "#" + hoaDon.MaHoaDon.ToString();
			this.lblTenKhachHang.Text = khachHang.HoTen;
			this.lblSDTKhachHang.Text = khachHang.SDT;
			this.lblTenNhanVien.Text = nhanVien.HoTen;
			this.lblEmailNhanVien.Text = nhanVien.Email;
			for (int i = listDichVu.Count - 1; i >= 0; i--)
			{
				TaoPanel(listDichVu[i], i);
			}
			TaoPanelDauMuc();
			this.lblTongTien.Text = hoaDon.TongTien.ToString();
			if (khachHang.KhachVIP == true)
			{
				lblKhuyenMai.Text = (hoaDon.TongTien * 0.1).ToString();
				lblGiaTienThanhToan.Text = (hoaDon.TongTien * 0.9).ToString();
			}
			else
			{
				lblKhuyenMai.Text = "0";
				lblGiaTienThanhToan.Text = hoaDon.TongTien.ToString();
			}
		}
		void TaoPanel(ChiTietDichVu dv, int i)
		{
			Panel panel = new Panel();
			this.panelMatHangSuDung.Controls.Add(panel);
			Label lblSTT1 = new Label();
			Label lblTongTien1 = new Label();
			Label lblSoLuong1 = new Label();
			Label lblMatHang1 = new Label();
			Label lblGia1 = new Label();
			panel.Controls.Add(lblTongTien1);
			panel.Controls.Add(lblGia1);
			panel.Controls.Add(lblSoLuong1);
			panel.Controls.Add(lblMatHang1);
			panel.Controls.Add(lblSTT1);
			panel.Dock = System.Windows.Forms.DockStyle.Top;
			panel.Size = new System.Drawing.Size(342, 36);
			panel.TabIndex = i + 1;
			// 
			// lblTongTien1
			// 
			lblTongTien1.BackColor = System.Drawing.Color.White;
			lblTongTien1.Dock = System.Windows.Forms.DockStyle.Left;
			lblTongTien1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			lblTongTien1.ForeColor = System.Drawing.Color.Black;
			lblTongTien1.Location = new System.Drawing.Point(224, 0);
			lblTongTien1.Size = new System.Drawing.Size(118, 36);
			lblTongTien1.TabIndex = 4;
			lblTongTien1.Text = dv.Gia.ToString();
			lblTongTien1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblGia1
			// 
			lblGia1.BackColor = System.Drawing.Color.White;
			lblGia1.Dock = System.Windows.Forms.DockStyle.Left;
			lblGia1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			lblGia1.ForeColor = System.Drawing.Color.Black;
			lblGia1.Location = new System.Drawing.Point(155, 0);
			lblGia1.Size = new System.Drawing.Size(69, 36);
			lblGia1.TabIndex = 3;
			lblGia1.Text = (dv.Gia / dv.SoLuong).ToString();
			lblGia1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSoLuong1
			// 
			lblSoLuong1.BackColor = System.Drawing.Color.White;
			lblSoLuong1.Dock = System.Windows.Forms.DockStyle.Left;
			lblSoLuong1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			lblSoLuong1.ForeColor = System.Drawing.Color.Black;
			lblSoLuong1.Location = new System.Drawing.Point(121, 0);
			lblSoLuong1.Size = new System.Drawing.Size(34, 36);
			lblSoLuong1.TabIndex = 2;
			lblSoLuong1.Text = dv.SoLuong.ToString();
			lblSoLuong1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblMatHang1
			// 
			lblMatHang1.BackColor = System.Drawing.Color.White;
			lblMatHang1.Dock = System.Windows.Forms.DockStyle.Left;
			lblMatHang1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			lblMatHang1.ForeColor = System.Drawing.Color.Black;
			lblMatHang1.Location = new System.Drawing.Point(45, 0);
			lblMatHang1.Size = new System.Drawing.Size(76, 36);
			lblMatHang1.TabIndex = 1;
			lblMatHang1.Text = dv.TenDV;
			lblMatHang1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSTT1
			// 
			lblSTT1.BackColor = System.Drawing.Color.White;
			lblSTT1.Dock = System.Windows.Forms.DockStyle.Left;
			lblSTT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			lblSTT1.ForeColor = System.Drawing.Color.Black;
			lblSTT1.Location = new System.Drawing.Point(0, 0);
			lblSTT1.Size = new System.Drawing.Size(45, 36);
			lblSTT1.TabIndex = 0;
			lblSTT1.Text = (i + 1).ToString();
			lblSTT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		}
		void TaoPanelDauMuc()
		{
			Panel panelDauMuc = new Panel();
			panelMatHangSuDung.Controls.Add(panelDauMuc);
			Label label18 = new Label();
			Label label16 = new Label();
			Label label15 = new Label();
			Label label14 = new Label();
			Label label17 = new Label();
			panelDauMuc.Controls.Add(label18);
			panelDauMuc.Controls.Add(label17);
			panelDauMuc.Controls.Add(label16);
			panelDauMuc.Controls.Add(label15);
			panelDauMuc.Controls.Add(label14);
			panelDauMuc.Dock = System.Windows.Forms.DockStyle.Top;
			panelDauMuc.Name = "panelDauMuc";
			panelDauMuc.Size = new System.Drawing.Size(342, 36);
			panelDauMuc.TabIndex = 0;
			label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			label14.Dock = System.Windows.Forms.DockStyle.Left;
			label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label14.ForeColor = System.Drawing.Color.White;
			label14.Location = new System.Drawing.Point(0, 0);
			label14.Name = "label14";
			label14.Size = new System.Drawing.Size(45, 36);
			label14.TabIndex = 0;
			label14.Text = "STT";
			label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			label15.Dock = System.Windows.Forms.DockStyle.Left;
			label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label15.ForeColor = System.Drawing.Color.White;
			label15.Location = new System.Drawing.Point(45, 0);
			label15.Name = "label15";
			label15.Size = new System.Drawing.Size(76, 36);
			label15.TabIndex = 1;
			label15.Text = "Mặt Hàng";
			label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label16
			// 
			label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			label16.Dock = System.Windows.Forms.DockStyle.Left;
			label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label16.ForeColor = System.Drawing.Color.White;
			label16.Location = new System.Drawing.Point(121, 0);
			label16.Name = "label16";
			label16.Size = new System.Drawing.Size(34, 36);
			label16.TabIndex = 2;
			label16.Text = "SL";
			label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label17
			// 
			label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			label17.Dock = System.Windows.Forms.DockStyle.Left;
			label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label17.ForeColor = System.Drawing.Color.White;
			label17.Location = new System.Drawing.Point(155, 0);
			label17.Name = "label17";
			label17.Size = new System.Drawing.Size(69, 36);
			label17.TabIndex = 3;
			label17.Text = "Giá";
			label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label18
			// 
			label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			label18.Dock = System.Windows.Forms.DockStyle.Left;
			label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label18.ForeColor = System.Drawing.Color.White;
			label18.Location = new System.Drawing.Point(224, 0);
			label18.Name = "label18";
			label18.Size = new System.Drawing.Size(118, 36);
			label18.TabIndex = 4;
			label18.Text = "Tổng Tiền";
			label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelDauMuc
			// 

		}

		private void button1_Click(object sender, EventArgs e)
		{
			PrintDocument pd = new PrintDocument();
			pd.PrintPage += Pd_PrintPage;

			// Hiển thị hộp thoại PrintDialog để người dùng chọn máy in
			PrintDialog printDialog = new PrintDialog();
			printDialog.Document = pd;

			if (printDialog.ShowDialog() == DialogResult.OK)
			{
				// In form lên giấy
				pd.Print();
			}
			this.Close();
		}
		private void Pd_PrintPage(object sender, PrintPageEventArgs e)
		{
			// Lấy hình ảnh của form
			Bitmap bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
			DrawToBitmap(bmp, new Rectangle(0, 0, ClientSize.Width, ClientSize.Height));

			// In hình ảnh lên giấy
			e.Graphics.DrawImage(bmp, 0, 0);
		}
	}
}
