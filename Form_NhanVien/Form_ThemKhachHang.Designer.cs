﻿namespace QLKS_ADONET.Form_NhanVien
{
	partial class Form_ThemKhachHang
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtCCCD = new System.Windows.Forms.TextBox();
			this.txtEmail = new System.Windows.Forms.TextBox();
			this.txtSDT = new System.Windows.Forms.TextBox();
			this.txtDiaChi = new System.Windows.Forms.TextBox();
			this.txtHoTen = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnThem = new System.Windows.Forms.Button();
			this.nudSoNguoiO = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.txtTienCoc = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.nudSoNgayO = new System.Windows.Forms.NumericUpDown();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudSoNguoiO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudSoNgayO)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.panel1.Controls.Add(this.pictureBox2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1083, 103);
			this.panel1.TabIndex = 0;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox2.Location = new System.Drawing.Point(1008, 26);
			this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(45, 46);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 2;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(34, 29);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(230, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "Thông tin khách hàng";
			// 
			// txtCCCD
			// 
			this.txtCCCD.BackColor = System.Drawing.Color.White;
			this.txtCCCD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtCCCD.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtCCCD.ForeColor = System.Drawing.Color.Black;
			this.txtCCCD.Location = new System.Drawing.Point(230, 562);
			this.txtCCCD.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtCCCD.Multiline = true;
			this.txtCCCD.Name = "txtCCCD";
			this.txtCCCD.Size = new System.Drawing.Size(266, 42);
			this.txtCCCD.TabIndex = 45;
			// 
			// txtEmail
			// 
			this.txtEmail.BackColor = System.Drawing.Color.White;
			this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtEmail.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtEmail.ForeColor = System.Drawing.Color.Black;
			this.txtEmail.Location = new System.Drawing.Point(230, 457);
			this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtEmail.Multiline = true;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(266, 42);
			this.txtEmail.TabIndex = 44;
			// 
			// txtSDT
			// 
			this.txtSDT.BackColor = System.Drawing.Color.White;
			this.txtSDT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSDT.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSDT.ForeColor = System.Drawing.Color.Black;
			this.txtSDT.Location = new System.Drawing.Point(230, 357);
			this.txtSDT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtSDT.Multiline = true;
			this.txtSDT.Name = "txtSDT";
			this.txtSDT.Size = new System.Drawing.Size(266, 42);
			this.txtSDT.TabIndex = 43;
			// 
			// txtDiaChi
			// 
			this.txtDiaChi.BackColor = System.Drawing.Color.White;
			this.txtDiaChi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtDiaChi.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDiaChi.ForeColor = System.Drawing.Color.Black;
			this.txtDiaChi.Location = new System.Drawing.Point(230, 255);
			this.txtDiaChi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtDiaChi.Multiline = true;
			this.txtDiaChi.Name = "txtDiaChi";
			this.txtDiaChi.Size = new System.Drawing.Size(266, 42);
			this.txtDiaChi.TabIndex = 42;
			// 
			// txtHoTen
			// 
			this.txtHoTen.BackColor = System.Drawing.Color.White;
			this.txtHoTen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtHoTen.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtHoTen.ForeColor = System.Drawing.Color.Black;
			this.txtHoTen.Location = new System.Drawing.Point(230, 154);
			this.txtHoTen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtHoTen.Multiline = true;
			this.txtHoTen.Name = "txtHoTen";
			this.txtHoTen.Size = new System.Drawing.Size(266, 42);
			this.txtHoTen.TabIndex = 41;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(40, 565);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(59, 22);
			this.label6.TabIndex = 40;
			this.label6.Text = "CCCD";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(40, 463);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(58, 22);
			this.label5.TabIndex = 39;
			this.label5.Text = "Email";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Location = new System.Drawing.Point(45, 360);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(43, 22);
			this.label4.TabIndex = 38;
			this.label4.Text = "SĐT";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(45, 258);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 22);
			this.label3.TabIndex = 37;
			this.label3.Text = "Địa chỉ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(45, 160);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 22);
			this.label2.TabIndex = 36;
			this.label2.Text = "Họ Tên";
			// 
			// btnThem
			// 
			this.btnThem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnThem.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnThem.FlatAppearance.BorderSize = 0;
			this.btnThem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThem.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThem.ForeColor = System.Drawing.Color.White;
			this.btnThem.Location = new System.Drawing.Point(762, 542);
			this.btnThem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnThem.Name = "btnThem";
			this.btnThem.Size = new System.Drawing.Size(266, 71);
			this.btnThem.TabIndex = 46;
			this.btnThem.Text = "THÊM";
			this.btnThem.UseVisualStyleBackColor = false;
			this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
			// 
			// nudSoNguoiO
			// 
			this.nudSoNguoiO.AllowDrop = true;
			this.nudSoNguoiO.BackColor = System.Drawing.Color.White;
			this.nudSoNguoiO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudSoNguoiO.ForeColor = System.Drawing.Color.Black;
			this.nudSoNguoiO.Location = new System.Drawing.Point(766, 260);
			this.nudSoNguoiO.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.nudSoNguoiO.Name = "nudSoNguoiO";
			this.nudSoNguoiO.ReadOnly = true;
			this.nudSoNguoiO.Size = new System.Drawing.Size(266, 26);
			this.nudSoNguoiO.TabIndex = 47;
			this.nudSoNguoiO.Value = new decimal(new int[] {
			1,
			0,
			0,
			0});
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Location = new System.Drawing.Point(584, 262);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(98, 22);
			this.label7.TabIndex = 48;
			this.label7.Text = "Số người ở";
			// 
			// txtTienCoc
			// 
			this.txtTienCoc.BackColor = System.Drawing.Color.White;
			this.txtTienCoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtTienCoc.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtTienCoc.ForeColor = System.Drawing.Color.Black;
			this.txtTienCoc.Location = new System.Drawing.Point(765, 155);
			this.txtTienCoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txtTienCoc.Multiline = true;
			this.txtTienCoc.Name = "txtTienCoc";
			this.txtTienCoc.Size = new System.Drawing.Size(266, 42);
			this.txtTienCoc.TabIndex = 50;
			this.txtTienCoc.Text = "0";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.Black;
			this.label8.Location = new System.Drawing.Point(584, 160);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(79, 22);
			this.label8.TabIndex = 49;
			this.label8.Text = "Tiền cọc";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.Color.Black;
			this.label9.Location = new System.Drawing.Point(584, 366);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(92, 22);
			this.label9.TabIndex = 52;
			this.label9.Text = "Số ngày ở";
			// 
			// nudSoNgayO
			// 
			this.nudSoNgayO.AllowDrop = true;
			this.nudSoNgayO.BackColor = System.Drawing.Color.White;
			this.nudSoNgayO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudSoNgayO.ForeColor = System.Drawing.Color.Black;
			this.nudSoNgayO.Location = new System.Drawing.Point(768, 362);
			this.nudSoNgayO.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.nudSoNgayO.Name = "nudSoNgayO";
			this.nudSoNgayO.ReadOnly = true;
			this.nudSoNgayO.Size = new System.Drawing.Size(266, 26);
			this.nudSoNgayO.TabIndex = 51;
			this.nudSoNgayO.Value = new decimal(new int[] {
			1,
			0,
			0,
			0});
			// 
			// Form_ThemKhachHang
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
			this.ClientSize = new System.Drawing.Size(1083, 665);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.nudSoNgayO);
			this.Controls.Add(this.txtTienCoc);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.nudSoNguoiO);
			this.Controls.Add(this.btnThem);
			this.Controls.Add(this.txtCCCD);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.txtEmail);
			this.Controls.Add(this.txtSDT);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtDiaChi);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtHoTen);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Cursor = System.Windows.Forms.Cursors.Hand;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "Form_ThemKhachHang";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form_ThemKhachHang";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudSoNguoiO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudSoNgayO)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.TextBox txtCCCD;
		private System.Windows.Forms.TextBox txtEmail;
		private System.Windows.Forms.TextBox txtSDT;
		private System.Windows.Forms.TextBox txtDiaChi;
		private System.Windows.Forms.TextBox txtHoTen;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnThem;
		private System.Windows.Forms.Label label7;
		public System.Windows.Forms.NumericUpDown nudSoNguoiO;
		private System.Windows.Forms.Label label8;
		public System.Windows.Forms.TextBox txtTienCoc;
		private System.Windows.Forms.Label label9;
		public System.Windows.Forms.NumericUpDown nudSoNgayO;
	}
}