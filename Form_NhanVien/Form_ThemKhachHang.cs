﻿using QLKS_ADONET.BS_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_NhanVien
{
	public partial class Form_ThemKhachHang : Form
	{
		public KhachHang khachHang { get; set; }
		public int soNguoiO { get; set; }
		public float tienCoc { get; set; }
		public int soNgayO { get; set; }

		public Form_ThemKhachHang()
		{
			InitializeComponent();
		}

		private void btnThem_Click(object sender, EventArgs e)
		{
			if (txtHoTen.Text == "" || txtDiaChi.Text == "" || txtSDT.Text == "" || txtEmail.Text == "" || txtCCCD.Text == "" ||
				!txtSDT.Text.All(char.IsDigit) || !txtEmail.Text.Contains("@gmail.com") || !txtCCCD.Text.All(char.IsDigit) || !txtTienCoc.Text.All(char.IsDigit))
			{
				MessageBox.Show("Thông tin không đầy đủ hoặc bạn đã nhập sai định dạng", "Không đủ thông tin", MessageBoxButtons.OK);
			}
			else
			{
				BL_KhachHang blkh = new BL_KhachHang();
				khachHang = blkh.ThemKhachHang(txtHoTen.Text.Trim(), txtDiaChi.Text.Trim(), txtEmail.Text.Trim(), txtSDT.Text.Trim(), txtCCCD.Text.Trim());
				tienCoc = Convert.ToInt32(this.txtTienCoc.Text.Trim().ToString());
				soNguoiO = Convert.ToInt32(this.nudSoNguoiO.Value);
				soNgayO = Convert.ToInt32(this.nudSoNgayO.Value);

				ValuePassed?.Invoke(khachHang, soNguoiO, tienCoc, soNgayO);

				MessageBox.Show("Thành công");
				this.Close();
			}
		}

		private void pictureBox2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		public delegate void ValuePassedDelegate(KhachHang khachHang, int songuoio, float tiencoc, int songayo);
		public event ValuePassedDelegate ValuePassed;
	}
}
