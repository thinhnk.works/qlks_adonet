﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLKS_ADONET.BS_Layer;

namespace QLKS_ADONET.Form_NhanVien
{
	public partial class Form_ThongTinKhachHang : Form
	{
		BL_KhachHang blKhachHang = new BL_KhachHang();
		bool Them;
		public Form_ThongTinKhachHang()
		{
			InitializeComponent();
		}
		private void pictureBox1_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		void LoadData()
		{
			try
			{
				// Đưa dữ liệu lên DataGridView
				dataGridView1.DataSource = blKhachHang.DanhSachKhachHang();
				// Thay đổi độ rộng cột
				dataGridView1.AutoResizeColumns();
				// Xóa trống các đối tượng trong Panel
				this.txtMaKH.ResetText();
				this.txtHoTen.ResetText();
				this.txtDiaChi.ResetText();
				this.txtSDT.ResetText();
				this.txtEmail.ResetText();
				this.txtCCCD.ResetText();
				this.txtSoLanNghi.ResetText();
				this.txtKhachVip.ResetText();
				// Không cho thao tác trên các nút Lưu / Hủy
				this.btnLuu.Enabled = false;
				this.btnHuyBo.Enabled = false;
				this.panel1.Enabled = false;
				// Cho thao tác trên các nút Thêm / Sửa / Xóa /Thoát
				this.btnSua.Enabled = true;
				this.btnThem.Enabled = true;
				this.btnXoa.Enabled = true;
				//
				//dataGridView1_CellClick(null, null);
			}
			catch (SqlException)
			{
				MessageBox.Show("Không lấy được nội dung trong table KhachHang. Lỗi rồi!!!");
			}
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			int r = dataGridView1.CurrentCell.RowIndex;
			// Chuyển thông tin lên panel
			this.txtMaKH.Text = dataGridView1.Rows[r].Cells[0].Value.ToString();
			this.txtHoTen.Text = dataGridView1.Rows[r].Cells[1].Value.ToString();
			this.txtDiaChi.Text = dataGridView1.Rows[r].Cells[2].Value.ToString();
			this.txtSDT.Text = dataGridView1.Rows[r].Cells[3].Value.ToString();
			this.txtEmail.Text = dataGridView1.Rows[r].Cells[5].Value.ToString();
			this.txtCCCD.Text = dataGridView1.Rows[r].Cells[4].Value.ToString();
			this.txtSoLanNghi.Text = dataGridView1.Rows[r].Cells[6].Value.ToString();
			this.txtKhachVip.Text = dataGridView1.Rows[r].Cells[7].Value.ToString();
		}

		private void Form_ThongTinKhachHang_Load(object sender, EventArgs e)
		{
			LoadData();
		}

		private void btnReload_Click(object sender, EventArgs e)
		{
			LoadData();
		}

		private void btnThem_Click(object sender, EventArgs e)
		{
			Them = true;
			// Xóa trống các đối tượng trong Panel
			this.txtMaKH.ResetText();
			this.txtHoTen.ResetText();
			this.txtDiaChi.ResetText();
			this.txtSDT.ResetText();
			this.txtEmail.ResetText();
			this.txtCCCD.ResetText();
			this.txtSoLanNghi.ResetText();
			this.txtKhachVip.ResetText();
			// Cho thao tác trên các nút Lưu / Hủy / Panel
			this.btnLuu.Enabled = true;
			this.btnHuyBo.Enabled = true;
			this.panel1.Enabled = true;
			// Không cho thao tác trên các nút Thêm / Xóa / Thoát
			this.btnThem.Enabled = false;
			this.btnXoa.Enabled = false;
			this.btnSua.Enabled = false;
			// Đưa con trỏ đến TextField txtThanhPho
			this.txtMaKH.Enabled = false;
			this.txtSoLanNghi.Enabled = false;
			this.txtKhachVip.Enabled = false;
			this.txtHoTen.Focus();
		}

		private void btnSua_Click(object sender, EventArgs e)
		{
			// Kích hoạt biến Sửa
			Them = false;
			// Cho phép thao tác trên Panel
			this.panel1.Enabled = true;
			dataGridView1_CellClick(null, null);
			// Cho thao tác trên các nút Lưu / Hủy / Panel
			this.btnLuu.Enabled = true;
			this.btnHuyBo.Enabled = true;
			this.panel1.Enabled = true;
			// Không cho thao tác trên các nút Thêm / Xóa / Thoát
			this.btnThem.Enabled = false;
			this.btnXoa.Enabled = false;
			this.btnSua.Enabled = false;
			// Đưa con trỏ đến TextField txtMaKH
			//this.txtMaKH.Enabled = false;
			this.txtMaKH.Enabled = false;
			this.txtSoLanNghi.Enabled = false;
			this.txtKhachVip.Enabled = false;
			this.txtHoTen.Focus();
		}

		private void btnLuu_Click(object sender, EventArgs e)
		{
			if (Them)
			{
				try
				{
					// Thực hiện lệnh
					BL_KhachHang blKH = new BL_KhachHang();
					blKH.ThemKhachHang(this.txtHoTen.Text.Trim(), this.txtDiaChi.Text.Trim(), this.txtEmail.Text.Trim(), this.txtSDT.Text.Trim(), this.txtCCCD.Text.Trim());
					// Load lại dữ liệu trên DataGridView
					LoadData();
					// Thông báo
					MessageBox.Show("Đã thêm xong!");
				}
				catch (SqlException)
				{
					MessageBox.Show("Không thêm được. Lỗi rồi!");
				}
			}
			else
			{
				// Thực hiện lệnh
				BL_KhachHang blKH = new BL_KhachHang();
				blKH.SuaKhachHang(Convert.ToInt32(this.txtMaKH.Text.Trim()), this.txtHoTen.Text.Trim(), this.txtDiaChi.Text.Trim(), this.txtEmail.Text.Trim(), this.txtSDT.Text.Trim(), this.txtCCCD.Text.Trim());
				LoadData();
				// Thông báo
				MessageBox.Show("Đã sửa xong!");
			}
		}

		private void btnXoa_Click(object sender, EventArgs e)
		{
			try
			{
				// Thực hiện lệnh
				// Lấy thứ tự record hiện hành
				int r = dataGridView1.CurrentCell.RowIndex;
				// Lấy MaKH của record hiện hành
				int MaKH = Convert.ToInt32(dataGridView1.Rows[r].Cells[0].Value.ToString());
				// Khai báo biến traloi
				DialogResult traloi;
				// Hiện hộp thoại hỏi đáp
				traloi = MessageBox.Show("Chắc xóa mẫu tin này không?", "Trả lời",
				MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				// Kiểm tra có nhắp chọn nút Ok không?
				if (traloi == DialogResult.Yes)
				{
					blKhachHang.XoaKhachHang(MaKH);
					// Cập nhật lại DataGridView
					LoadData();
				}
				else
				{
					// Thông báo
					MessageBox.Show("Không thực hiện việc xóa mẫu tin!");
				}
			}
			catch (SqlException)
			{
				MessageBox.Show("Không xóa được. Lỗi rồi!");

			}
		}

		private void btnHuyBo_Click(object sender, EventArgs e)
		{
			this.txtMaKH.ResetText();
			this.txtHoTen.ResetText();
			this.txtDiaChi.ResetText();
			this.txtSDT.ResetText();
			this.txtEmail.ResetText();
			this.txtCCCD.ResetText();
			this.txtSoLanNghi.ResetText();
			this.txtKhachVip.ResetText();
			// Cho thao tác trên các nút Thêm / Sửa / Xóa / Thoát
			this.btnThem.Enabled = true;
			this.btnSua.Enabled = true;
			this.btnXoa.Enabled = true;
			// Không cho thao tác trên các nút Lưu / Hủy / Panel
			this.btnLuu.Enabled = false;
			this.btnHuyBo.Enabled = false;
			this.panel1.Enabled = false;
			dataGridView1_CellClick(null, null);
		}

		private void btnTroVe_Click(object sender, EventArgs e)
		{
			DialogResult traloi;
			// Hiện hộp thoại hỏi đáp
			traloi = MessageBox.Show("Chắc không?", "Trả lời",
			MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
			// Kiểm tra có nhắp chọn nút Ok không?
			if (traloi == DialogResult.OK) this.Close();

		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{

		}
		private void btnSearch_Click(object sender, EventArgs e)
		{
			if (txtSearch.Text != "")
			{
				dataGridView1.DataSource = blKhachHang.TimKiemKhachHang(txtSearch.Text.Trim());
			}
			else
			{
				dataGridView1.DataSource = blKhachHang.DanhSachKhachHang();
			}
		}
	}
}
