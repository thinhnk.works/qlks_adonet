﻿namespace QLKS_ADONET.Form_QuanLy
{
	partial class Form_Admin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lblText = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.lblUsername = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.btnLichSuDatPhong = new System.Windows.Forms.Button();
			this.btnChinhSuaNhanVien = new System.Windows.Forms.Button();
			this.btnChinhSuaKhachHang = new System.Windows.Forms.Button();
			this.btnChinhSuaDichVu = new System.Windows.Forms.Button();
			this.btnChinhSua = new System.Windows.Forms.Button();
			this.btn_ThongKe = new System.Windows.Forms.Button();
			this.btn_Report = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(64)))), ((int)(((byte)(103)))));
			this.panel1.Controls.Add(this.lblText);
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Controls.Add(this.lblUsername);
			this.panel1.Controls.Add(this.pictureBox2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1216, 54);
			this.panel1.TabIndex = 0;
			// 
			// lblText
			// 
			this.lblText.AutoSize = true;
			this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(161)))), ((int)(((byte)(178)))));
			this.lblText.Location = new System.Drawing.Point(68, 30);
			this.lblText.Name = "lblText";
			this.lblText.Size = new System.Drawing.Size(179, 20);
			this.lblText.TabIndex = 12;
			this.lblText.Text = "Some user text here";
			this.lblText.Click += new System.EventHandler(this.lblText_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox1.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox1.Location = new System.Drawing.Point(1174, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(35, 30);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// lblUsername
			// 
			this.lblUsername.AutoSize = true;
			this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
			this.lblUsername.ForeColor = System.Drawing.Color.White;
			this.lblUsername.Location = new System.Drawing.Point(68, 9);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(119, 25);
			this.lblUsername.TabIndex = 11;
			this.lblUsername.Text = "User Name";
			this.lblUsername.Click += new System.EventHandler(this.lblUsername_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::QLKS_ADONET.Properties.Resources.man;
			this.pictureBox2.Location = new System.Drawing.Point(3, 1);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(51, 50);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 10;
			this.pictureBox2.TabStop = false;
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.dataGridView1.ColumnHeadersHeight = 25;
			this.dataGridView1.EnableHeadersVisualStyles = false;
			this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.dataGridView1.Location = new System.Drawing.Point(225, 70);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidth = 62;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(979, 571);
			this.dataGridView1.TabIndex = 2;
			// 
			// btnLichSuDatPhong
			// 
			this.btnLichSuDatPhong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnLichSuDatPhong.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnLichSuDatPhong.FlatAppearance.BorderSize = 0;
			this.btnLichSuDatPhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLichSuDatPhong.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLichSuDatPhong.ForeColor = System.Drawing.Color.White;
			this.btnLichSuDatPhong.Location = new System.Drawing.Point(31, 82);
			this.btnLichSuDatPhong.Name = "btnLichSuDatPhong";
			this.btnLichSuDatPhong.Size = new System.Drawing.Size(164, 53);
			this.btnLichSuDatPhong.TabIndex = 4;
			this.btnLichSuDatPhong.Text = "Xem lịch sử";
			this.btnLichSuDatPhong.UseVisualStyleBackColor = false;
			this.btnLichSuDatPhong.Click += new System.EventHandler(this.btnLichSuDatPhong_Click);
			// 
			// btnChinhSuaNhanVien
			// 
			this.btnChinhSuaNhanVien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnChinhSuaNhanVien.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChinhSuaNhanVien.FlatAppearance.BorderSize = 0;
			this.btnChinhSuaNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSuaNhanVien.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSuaNhanVien.ForeColor = System.Drawing.Color.White;
			this.btnChinhSuaNhanVien.Location = new System.Drawing.Point(31, 165);
			this.btnChinhSuaNhanVien.Name = "btnChinhSuaNhanVien";
			this.btnChinhSuaNhanVien.Size = new System.Drawing.Size(164, 53);
			this.btnChinhSuaNhanVien.TabIndex = 5;
			this.btnChinhSuaNhanVien.Text = "Nhân Viên";
			this.btnChinhSuaNhanVien.UseVisualStyleBackColor = false;
			this.btnChinhSuaNhanVien.Click += new System.EventHandler(this.btnChinhSuaNhanVien_Click);
			// 
			// btnChinhSuaKhachHang
			// 
			this.btnChinhSuaKhachHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnChinhSuaKhachHang.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChinhSuaKhachHang.FlatAppearance.BorderSize = 0;
			this.btnChinhSuaKhachHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSuaKhachHang.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSuaKhachHang.ForeColor = System.Drawing.Color.White;
			this.btnChinhSuaKhachHang.Location = new System.Drawing.Point(31, 247);
			this.btnChinhSuaKhachHang.Name = "btnChinhSuaKhachHang";
			this.btnChinhSuaKhachHang.Size = new System.Drawing.Size(164, 53);
			this.btnChinhSuaKhachHang.TabIndex = 6;
			this.btnChinhSuaKhachHang.Text = "Khách Hàng";
			this.btnChinhSuaKhachHang.UseVisualStyleBackColor = false;
			this.btnChinhSuaKhachHang.Click += new System.EventHandler(this.btnChinhSuaKhachHang_Click);
			// 
			// btnChinhSuaDichVu
			// 
			this.btnChinhSuaDichVu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btnChinhSuaDichVu.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChinhSuaDichVu.FlatAppearance.BorderSize = 0;
			this.btnChinhSuaDichVu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSuaDichVu.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSuaDichVu.ForeColor = System.Drawing.Color.White;
			this.btnChinhSuaDichVu.Location = new System.Drawing.Point(31, 326);
			this.btnChinhSuaDichVu.Name = "btnChinhSuaDichVu";
			this.btnChinhSuaDichVu.Size = new System.Drawing.Size(164, 53);
			this.btnChinhSuaDichVu.TabIndex = 7;
			this.btnChinhSuaDichVu.Text = "Dịch Vụ";
			this.btnChinhSuaDichVu.UseVisualStyleBackColor = false;
			this.btnChinhSuaDichVu.Click += new System.EventHandler(this.btnChinhSuaDichVu_Click);
			// 
			// btnChinhSua
			// 
			this.btnChinhSua.BackColor = System.Drawing.Color.LimeGreen;
			this.btnChinhSua.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChinhSua.FlatAppearance.BorderSize = 3;
			this.btnChinhSua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSua.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSua.ForeColor = System.Drawing.Color.White;
			this.btnChinhSua.Location = new System.Drawing.Point(31, 499);
			this.btnChinhSua.Name = "btnChinhSua";
			this.btnChinhSua.Size = new System.Drawing.Size(164, 60);
			this.btnChinhSua.TabIndex = 9;
			this.btnChinhSua.Text = "Chinh Sửa";
			this.btnChinhSua.UseVisualStyleBackColor = false;
			this.btnChinhSua.Click += new System.EventHandler(this.btnChinhSua_Click);
			// 
			// btn_ThongKe
			// 
			this.btn_ThongKe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(169)))), ((int)(((byte)(252)))));
			this.btn_ThongKe.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_ThongKe.FlatAppearance.BorderSize = 0;
			this.btn_ThongKe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_ThongKe.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_ThongKe.ForeColor = System.Drawing.Color.White;
			this.btn_ThongKe.Location = new System.Drawing.Point(31, 406);
			this.btn_ThongKe.Name = "btn_ThongKe";
			this.btn_ThongKe.Size = new System.Drawing.Size(164, 53);
			this.btn_ThongKe.TabIndex = 11;
			this.btn_ThongKe.Text = "Thống kê";
			this.btn_ThongKe.UseVisualStyleBackColor = false;
			this.btn_ThongKe.Click += new System.EventHandler(this.btn_ThongKe_Click);
			// 
			// btn_Report
			// 
			this.btn_Report.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_Report.Location = new System.Drawing.Point(31, 576);
			this.btn_Report.Name = "btn_Report";
			this.btn_Report.Size = new System.Drawing.Size(164, 60);
			this.btn_Report.TabIndex = 12;
			this.btn_Report.Text = "Report";
			this.btn_Report.UseVisualStyleBackColor = true;
			this.btn_Report.Click += new System.EventHandler(this.btn_Report_Click);
			// 
			// Form_Admin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 33F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1216, 656);
			this.Controls.Add(this.btn_Report);
			this.Controls.Add(this.btn_ThongKe);
			this.Controls.Add(this.btnChinhSua);
			this.Controls.Add(this.btnChinhSuaDichVu);
			this.Controls.Add(this.btnChinhSuaKhachHang);
			this.Controls.Add(this.btnChinhSuaNhanVien);
			this.Controls.Add(this.btnLichSuDatPhong);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "Form_Admin";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form_QuanLy";
			this.Load += new System.EventHandler(this.Form_Admin_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnLichSuDatPhong;
        private System.Windows.Forms.Button btnChinhSuaNhanVien;
        private System.Windows.Forms.Button btnChinhSuaKhachHang;
        private System.Windows.Forms.Button btnChinhSuaDichVu;
        private System.Windows.Forms.Button btnChinhSua;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btn_ThongKe;
		private System.Windows.Forms.Button btn_Report;
	}
}