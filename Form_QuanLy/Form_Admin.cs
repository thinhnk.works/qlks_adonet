﻿using QLKS_ADONET.BS_Layer;
using QLKS_ADONET.Form_NhanVien;
using QLKS_ADONET.Form_Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_QuanLy
{
	public partial class Form_Admin : Form
	{
		BL_DatPhong bldatphong = new BL_DatPhong();
		int flag = 0;
		Dictionary<int, dynamic> data = new Dictionary<int, dynamic>();
		public NhanVien nhanvien;
		public Form_Admin()
		{
			InitializeComponent();
			data.Add(1, new TaoNhanVien());
			data.Add(2, new Form_ThongTinKhachHang());
			data.Add(3, new TaoDichVu());


		}
		void LoadData(DataTable dttable)
		{
			dataGridView1.DataSource = null;
			try
			{
				dataGridView1.DataSource = dttable;
				dataGridView1.AutoResizeColumns();
			}
			catch (SqlException)
			{
				MessageBox.Show("Không lấy được nội dung trong table. Lỗi rồi!!!");
			}
		}
		void LoadData()
		{
			if (flag != 0 && flag != 4)
			{
				if (flag == 1)
					LoadData(new BL_NhanVien().LayDSNhanVien());
				else if (flag == 2)
					LoadData(new BL_KhachHang().DanhSachKhachHang());
				else if (flag == 3)
					LoadData(new BL_DichVu().DSDichVu());
			}
		}

		private void btnLichSuDatPhong_Click(object sender, EventArgs e)
		{
			flag = 0;
			LoadData(new BL_DatPhong().LSDatPhong());
		}

		private void btnChinhSuaNhanVien_Click(object sender, EventArgs e)
		{
			flag = 1;
			LoadData(new BL_NhanVien().LayDSNhanVien());
		}

		private void btnChinhSuaKhachHang_Click(object sender, EventArgs e)
		{

			flag = 2;
			LoadData(new BL_KhachHang().DanhSachKhachHang());
		}

		private void btnChinhSuaDichVu_Click(object sender, EventArgs e)
		{
			flag = 3;
			LoadData(new BL_DichVu().DSDichVu());
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void btnChinhSua_Click(object sender, EventArgs e)
		{
			if (flag != 0 && flag != 4)
			{
				data[flag].ShowDialog();
				//LoadData();
			}
		}

		private void lblUsername_Click(object sender, EventArgs e)
		{

		}

		private void lblText_Click(object sender, EventArgs e)
		{

		}

		private void Form_Admin_Load(object sender, EventArgs e)
		{
			lblUsername.Text = nhanvien.HoTen;
			lblText.Text = nhanvien.Email;
		}

		private void btn_ThongKe_Click(object sender, EventArgs e)
		{
			flag = 4;
			Form_ThongKe form = new Form_ThongKe();
			form.ShowDialog();
		}

		private void btn_Report_Click(object sender, EventArgs e)
		{
			Form_ReportDoanhThu form = new Form_ReportDoanhThu();
			form.ShowDialog();
		}
	}
}
