﻿using QLKS_ADONET.BS_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace QLKS_ADONET.Form_QuanLy
{
	public partial class Form_ThongKe : Form
	{
		public Form_ThongKe()
		{
			InitializeComponent();
			Axis axisX = new Axis();
			Axis axisY = new Axis();
			axisX.Title = "Tên Phòng";
			axisY.Title = "Tiền (VNĐ) ";
			chartDoanhThu.ChartAreas[0].AxisX = axisX;
			chartDoanhThu.ChartAreas[0].AxisY = axisY;
			chartDoanhThu.ChartAreas[0].AxisX.Interval = 1;
		}

		private void Form_ThongKe_Load(object sender, EventArgs e)
		{
			BL_ThongKe blthongke = new BL_ThongKe();
			var doanhthutungphong = blthongke.DoanhThuTungPhong();
			foreach (var doanhthu in doanhthutungphong)
			{
				chartDoanhThu.Series["Doanh thu từng phòng"].Points.AddXY(doanhthu.Key, doanhthu.Value);
			}
			lblDoanhThu1Thang.Text = blthongke.DoanhThu1ThangGanNhat().ToString() + " VNĐ";
			lblTongDoanhThu.Text = blthongke.TongDoanhThu().ToString() + " VNĐ";
		}

		private void pictureBox3_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
