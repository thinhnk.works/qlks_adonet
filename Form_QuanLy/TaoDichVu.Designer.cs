﻿
namespace QLKS_ADONET.Form_QuanLy
{
	partial class TaoDichVu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.panel1 = new System.Windows.Forms.Panel();
			this.txt_TenDV = new System.Windows.Forms.TextBox();
			this.txt_Gia = new System.Windows.Forms.TextBox();
			this.txt_MaDV = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lbl_TenDichVu = new System.Windows.Forms.Label();
			this.lbl_MaDichVu = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.btn_Them = new System.Windows.Forms.Button();
			this.btn_Sua = new System.Windows.Forms.Button();
			this.btn_Luu = new System.Windows.Forms.Button();
			this.btn_Huy = new System.Windows.Forms.Button();
			this.btn_Xoa = new System.Windows.Forms.Button();
			this.btn_TroVe = new System.Windows.Forms.Button();
			this.btn_Reload = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.btnSearch = new System.Windows.Forms.Button();
			this.txtSearch = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(205)))), ((int)(((byte)(217)))));
			this.panel1.Controls.Add(this.txt_TenDV);
			this.panel1.Controls.Add(this.txt_Gia);
			this.panel1.Controls.Add(this.txt_MaDV);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.lbl_TenDichVu);
			this.panel1.Controls.Add(this.lbl_MaDichVu);
			this.panel1.ForeColor = System.Drawing.Color.Black;
			this.panel1.Location = new System.Drawing.Point(10, 68);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(309, 395);
			this.panel1.TabIndex = 0;
			// 
			// txt_TenDV
			// 
			this.txt_TenDV.BackColor = System.Drawing.Color.White;
			this.txt_TenDV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_TenDV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_TenDV.Location = new System.Drawing.Point(14, 185);
			this.txt_TenDV.Multiline = true;
			this.txt_TenDV.Name = "txt_TenDV";
			this.txt_TenDV.Size = new System.Drawing.Size(287, 39);
			this.txt_TenDV.TabIndex = 5;
			// 
			// txt_Gia
			// 
			this.txt_Gia.BackColor = System.Drawing.Color.White;
			this.txt_Gia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_Gia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_Gia.Location = new System.Drawing.Point(12, 291);
			this.txt_Gia.Multiline = true;
			this.txt_Gia.Name = "txt_Gia";
			this.txt_Gia.Size = new System.Drawing.Size(287, 39);
			this.txt_Gia.TabIndex = 4;
			// 
			// txt_MaDV
			// 
			this.txt_MaDV.BackColor = System.Drawing.Color.White;
			this.txt_MaDV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_MaDV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_MaDV.Location = new System.Drawing.Point(12, 78);
			this.txt_MaDV.Multiline = true;
			this.txt_MaDV.Name = "txt_MaDV";
			this.txt_MaDV.Size = new System.Drawing.Size(287, 39);
			this.txt_MaDV.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(9, 262);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 25);
			this.label1.TabIndex = 2;
			this.label1.Text = "Giá:";
			// 
			// lbl_TenDichVu
			// 
			this.lbl_TenDichVu.AutoSize = true;
			this.lbl_TenDichVu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_TenDichVu.ForeColor = System.Drawing.Color.Black;
			this.lbl_TenDichVu.Location = new System.Drawing.Point(9, 157);
			this.lbl_TenDichVu.Name = "lbl_TenDichVu";
			this.lbl_TenDichVu.Size = new System.Drawing.Size(139, 25);
			this.lbl_TenDichVu.TabIndex = 1;
			this.lbl_TenDichVu.Text = "Tên Dịch Vụ:";
			// 
			// lbl_MaDichVu
			// 
			this.lbl_MaDichVu.AutoSize = true;
			this.lbl_MaDichVu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_MaDichVu.ForeColor = System.Drawing.Color.Black;
			this.lbl_MaDichVu.Location = new System.Drawing.Point(9, 49);
			this.lbl_MaDichVu.Name = "lbl_MaDichVu";
			this.lbl_MaDichVu.Size = new System.Drawing.Size(131, 25);
			this.lbl_MaDichVu.TabIndex = 0;
			this.lbl_MaDichVu.Text = "Mã Dịch Vụ:";
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.EnableHeadersVisualStyles = false;
			this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.dataGridView1.Location = new System.Drawing.Point(330, 117);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidth = 62;
			this.dataGridView1.RowTemplate.Height = 28;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(333, 552);
			this.dataGridView1.TabIndex = 1;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
			// 
			// btn_Them
			// 
			this.btn_Them.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Them.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Them.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Them.ForeColor = System.Drawing.Color.White;
			this.btn_Them.Location = new System.Drawing.Point(10, 500);
			this.btn_Them.Name = "btn_Them";
			this.btn_Them.Size = new System.Drawing.Size(94, 40);
			this.btn_Them.TabIndex = 2;
			this.btn_Them.Text = "Thêm ";
			this.btn_Them.UseVisualStyleBackColor = false;
			this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
			// 
			// btn_Sua
			// 
			this.btn_Sua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Sua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Sua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Sua.ForeColor = System.Drawing.Color.White;
			this.btn_Sua.Location = new System.Drawing.Point(225, 500);
			this.btn_Sua.Name = "btn_Sua";
			this.btn_Sua.Size = new System.Drawing.Size(94, 40);
			this.btn_Sua.TabIndex = 3;
			this.btn_Sua.Text = "Sửa";
			this.btn_Sua.UseVisualStyleBackColor = false;
			this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
			// 
			// btn_Luu
			// 
			this.btn_Luu.BackColor = System.Drawing.Color.LimeGreen;
			this.btn_Luu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Luu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Luu.ForeColor = System.Drawing.Color.White;
			this.btn_Luu.Location = new System.Drawing.Point(10, 568);
			this.btn_Luu.Name = "btn_Luu";
			this.btn_Luu.Size = new System.Drawing.Size(94, 40);
			this.btn_Luu.TabIndex = 4;
			this.btn_Luu.Text = "Lưu";
			this.btn_Luu.UseVisualStyleBackColor = false;
			this.btn_Luu.Click += new System.EventHandler(this.btn_Luu_Click);
			// 
			// btn_Huy
			// 
			this.btn_Huy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Huy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Huy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Huy.ForeColor = System.Drawing.Color.White;
			this.btn_Huy.Location = new System.Drawing.Point(230, 568);
			this.btn_Huy.Name = "btn_Huy";
			this.btn_Huy.Size = new System.Drawing.Size(94, 40);
			this.btn_Huy.TabIndex = 5;
			this.btn_Huy.Text = "Hủy";
			this.btn_Huy.UseVisualStyleBackColor = false;
			this.btn_Huy.Click += new System.EventHandler(this.btn_Huy_Click);
			// 
			// btn_Xoa
			// 
			this.btn_Xoa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Xoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Xoa.ForeColor = System.Drawing.Color.White;
			this.btn_Xoa.Location = new System.Drawing.Point(117, 500);
			this.btn_Xoa.Name = "btn_Xoa";
			this.btn_Xoa.Size = new System.Drawing.Size(94, 40);
			this.btn_Xoa.TabIndex = 6;
			this.btn_Xoa.Text = "Xóa ";
			this.btn_Xoa.UseVisualStyleBackColor = false;
			this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
			// 
			// btn_TroVe
			// 
			this.btn_TroVe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_TroVe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_TroVe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_TroVe.ForeColor = System.Drawing.Color.White;
			this.btn_TroVe.Location = new System.Drawing.Point(15, 629);
			this.btn_TroVe.Name = "btn_TroVe";
			this.btn_TroVe.Size = new System.Drawing.Size(309, 40);
			this.btn_TroVe.TabIndex = 7;
			this.btn_TroVe.Text = "Trở về";
			this.btn_TroVe.UseVisualStyleBackColor = false;
			this.btn_TroVe.Click += new System.EventHandler(this.btn_TroVe_Click);
			// 
			// btn_Reload
			// 
			this.btn_Reload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Reload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Reload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Reload.ForeColor = System.Drawing.Color.White;
			this.btn_Reload.Location = new System.Drawing.Point(117, 568);
			this.btn_Reload.Name = "btn_Reload";
			this.btn_Reload.Size = new System.Drawing.Size(94, 40);
			this.btn_Reload.TabIndex = 8;
			this.btn_Reload.Text = "Reload";
			this.btn_Reload.UseVisualStyleBackColor = false;
			this.btn_Reload.Click += new System.EventHandler(this.btn_Reload_Click);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.panel2.Controls.Add(this.pictureBox2);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(678, 54);
			this.panel2.TabIndex = 9;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox2.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox2.Location = new System.Drawing.Point(633, 11);
			this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(30, 31);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 2;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// btnSearch
			// 
			this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSearch.ForeColor = System.Drawing.Color.White;
			this.btnSearch.Location = new System.Drawing.Point(546, 68);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(117, 46);
			this.btnSearch.TabIndex = 20;
			this.btnSearch.Text = "Search";
			this.btnSearch.UseVisualStyleBackColor = false;
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.Color.White;
			this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSearch.Location = new System.Drawing.Point(330, 68);
			this.txtSearch.Multiline = true;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(192, 42);
			this.txtSearch.TabIndex = 19;
			// 
			// TaoDichVu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(678, 686);
			this.Controls.Add(this.btnSearch);
			this.Controls.Add(this.txtSearch);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.btn_Reload);
			this.Controls.Add(this.btn_TroVe);
			this.Controls.Add(this.btn_Xoa);
			this.Controls.Add(this.btn_Huy);
			this.Controls.Add(this.btn_Luu);
			this.Controls.Add(this.btn_Sua);
			this.Controls.Add(this.btn_Them);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "TaoDichVu";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TaoDichVu";
			this.Load += new System.EventHandler(this.TaoDichVu_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox txt_TenDV;
		private System.Windows.Forms.TextBox txt_Gia;
		private System.Windows.Forms.TextBox txt_MaDV;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbl_TenDichVu;
		private System.Windows.Forms.Label lbl_MaDichVu;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button btn_Them;
		private System.Windows.Forms.Button btn_Sua;
		private System.Windows.Forms.Button btn_Luu;
		private System.Windows.Forms.Button btn_Huy;
		private System.Windows.Forms.Button btn_Xoa;
		private System.Windows.Forms.Button btn_TroVe;
		private System.Windows.Forms.Button btn_Reload;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.TextBox txtSearch;
	}
}