﻿
namespace QLKS_ADONET.Form_QuanLy
{
	partial class TaoNhanVien
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lbl_MaNV = new System.Windows.Forms.Label();
			this.lbl_HoTen = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lbl_SDT = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lbMaNhanVien = new System.Windows.Forms.Label();
			this.txt_MatKhau = new System.Windows.Forms.TextBox();
			this.txt_Mail = new System.Windows.Forms.TextBox();
			this.txt_ChucVu = new System.Windows.Forms.TextBox();
			this.txt_SDT = new System.Windows.Forms.TextBox();
			this.txt_DiaChi = new System.Windows.Forms.TextBox();
			this.txt_HoTen = new System.Windows.Forms.TextBox();
			this.txtSearch = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.btn_Them = new System.Windows.Forms.Button();
			this.btn_Sua = new System.Windows.Forms.Button();
			this.btn_Luu = new System.Windows.Forms.Button();
			this.btn_Huy = new System.Windows.Forms.Button();
			this.btn_TroVe = new System.Windows.Forms.Button();
			this.btn_Xoa = new System.Windows.Forms.Button();
			this.btn_Reload = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.btnSearch = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// lbl_MaNV
			// 
			this.lbl_MaNV.AutoSize = true;
			this.lbl_MaNV.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_MaNV.Location = new System.Drawing.Point(17, 41);
			this.lbl_MaNV.Name = "lbl_MaNV";
			this.lbl_MaNV.Size = new System.Drawing.Size(97, 29);
			this.lbl_MaNV.TabIndex = 1;
			this.lbl_MaNV.Text = "Mã NV:";
			// 
			// lbl_HoTen
			// 
			this.lbl_HoTen.AutoSize = true;
			this.lbl_HoTen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_HoTen.Location = new System.Drawing.Point(16, 97);
			this.lbl_HoTen.Name = "lbl_HoTen";
			this.lbl_HoTen.Size = new System.Drawing.Size(139, 29);
			this.lbl_HoTen.TabIndex = 2;
			this.lbl_HoTen.Text = "Họ và Tên:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 328);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(105, 29);
			this.label4.TabIndex = 3;
			this.label4.Text = "Địa Chỉ:";
			// 
			// lbl_SDT
			// 
			this.lbl_SDT.AutoSize = true;
			this.lbl_SDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_SDT.Location = new System.Drawing.Point(16, 174);
			this.lbl_SDT.Name = "lbl_SDT";
			this.lbl_SDT.Size = new System.Drawing.Size(72, 29);
			this.lbl_SDT.TabIndex = 4;
			this.lbl_SDT.Text = "SĐT:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 405);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(86, 29);
			this.label6.TabIndex = 5;
			this.label6.Text = "Email:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(16, 251);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(128, 29);
			this.label7.TabIndex = 6;
			this.label7.Text = "Mật Khẩu:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(16, 485);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(116, 29);
			this.label8.TabIndex = 7;
			this.label8.Text = "Chức Vụ:";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(205)))), ((int)(((byte)(217)))));
			this.panel1.Controls.Add(this.lbMaNhanVien);
			this.panel1.Controls.Add(this.txt_MatKhau);
			this.panel1.Controls.Add(this.txt_Mail);
			this.panel1.Controls.Add(this.txt_ChucVu);
			this.panel1.Controls.Add(this.txt_SDT);
			this.panel1.Controls.Add(this.txt_DiaChi);
			this.panel1.Controls.Add(this.txt_HoTen);
			this.panel1.Controls.Add(this.lbl_MaNV);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.lbl_SDT);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.lbl_HoTen);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Location = new System.Drawing.Point(10, 60);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(316, 580);
			this.panel1.TabIndex = 8;
			// 
			// lbMaNhanVien
			// 
			this.lbMaNhanVien.AutoSize = true;
			this.lbMaNhanVien.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbMaNhanVien.Location = new System.Drawing.Point(122, 37);
			this.lbMaNhanVien.Name = "lbMaNhanVien";
			this.lbMaNhanVien.Size = new System.Drawing.Size(31, 33);
			this.lbMaNhanVien.TabIndex = 15;
			this.lbMaNhanVien.Text = "0";
			// 
			// txt_MatKhau
			// 
			this.txt_MatKhau.BackColor = System.Drawing.Color.White;
			this.txt_MatKhau.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_MatKhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_MatKhau.Location = new System.Drawing.Point(21, 282);
			this.txt_MatKhau.Multiline = true;
			this.txt_MatKhau.Name = "txt_MatKhau";
			this.txt_MatKhau.Size = new System.Drawing.Size(266, 42);
			this.txt_MatKhau.TabIndex = 14;
			// 
			// txt_Mail
			// 
			this.txt_Mail.BackColor = System.Drawing.Color.White;
			this.txt_Mail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_Mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_Mail.Location = new System.Drawing.Point(21, 438);
			this.txt_Mail.Multiline = true;
			this.txt_Mail.Name = "txt_Mail";
			this.txt_Mail.Size = new System.Drawing.Size(266, 42);
			this.txt_Mail.TabIndex = 13;
			// 
			// txt_ChucVu
			// 
			this.txt_ChucVu.BackColor = System.Drawing.Color.White;
			this.txt_ChucVu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_ChucVu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_ChucVu.Location = new System.Drawing.Point(20, 515);
			this.txt_ChucVu.Multiline = true;
			this.txt_ChucVu.Name = "txt_ChucVu";
			this.txt_ChucVu.Size = new System.Drawing.Size(266, 42);
			this.txt_ChucVu.TabIndex = 12;
			// 
			// txt_SDT
			// 
			this.txt_SDT.BackColor = System.Drawing.Color.White;
			this.txt_SDT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_SDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_SDT.Location = new System.Drawing.Point(20, 205);
			this.txt_SDT.Multiline = true;
			this.txt_SDT.Name = "txt_SDT";
			this.txt_SDT.Size = new System.Drawing.Size(266, 42);
			this.txt_SDT.TabIndex = 11;
			// 
			// txt_DiaChi
			// 
			this.txt_DiaChi.BackColor = System.Drawing.Color.White;
			this.txt_DiaChi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_DiaChi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_DiaChi.Location = new System.Drawing.Point(21, 358);
			this.txt_DiaChi.Multiline = true;
			this.txt_DiaChi.Name = "txt_DiaChi";
			this.txt_DiaChi.Size = new System.Drawing.Size(266, 42);
			this.txt_DiaChi.TabIndex = 10;
			// 
			// txt_HoTen
			// 
			this.txt_HoTen.BackColor = System.Drawing.Color.White;
			this.txt_HoTen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_HoTen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_HoTen.Location = new System.Drawing.Point(20, 128);
			this.txt_HoTen.Multiline = true;
			this.txt_HoTen.Name = "txt_HoTen";
			this.txt_HoTen.Size = new System.Drawing.Size(266, 42);
			this.txt_HoTen.TabIndex = 9;
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.Color.White;
			this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSearch.Location = new System.Drawing.Point(676, 62);
			this.txtSearch.Multiline = true;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(266, 42);
			this.txtSearch.TabIndex = 8;
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridView1.ColumnHeadersHeight = 25;
			this.dataGridView1.EnableHeadersVisualStyles = false;
			this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.dataGridView1.Location = new System.Drawing.Point(333, 126);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidth = 62;
			this.dataGridView1.RowTemplate.Height = 28;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(752, 328);
			this.dataGridView1.TabIndex = 9;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
			// 
			// btn_Them
			// 
			this.btn_Them.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Them.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Them.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Them.ForeColor = System.Drawing.Color.White;
			this.btn_Them.Location = new System.Drawing.Point(333, 482);
			this.btn_Them.Name = "btn_Them";
			this.btn_Them.Size = new System.Drawing.Size(160, 66);
			this.btn_Them.TabIndex = 10;
			this.btn_Them.Text = "Thêm ";
			this.btn_Them.UseVisualStyleBackColor = false;
			this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
			// 
			// btn_Sua
			// 
			this.btn_Sua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Sua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Sua.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Sua.ForeColor = System.Drawing.Color.White;
			this.btn_Sua.Location = new System.Drawing.Point(525, 482);
			this.btn_Sua.Name = "btn_Sua";
			this.btn_Sua.Size = new System.Drawing.Size(160, 66);
			this.btn_Sua.TabIndex = 11;
			this.btn_Sua.Text = "Sửa ";
			this.btn_Sua.UseVisualStyleBackColor = false;
			this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
			// 
			// btn_Luu
			// 
			this.btn_Luu.BackColor = System.Drawing.Color.LimeGreen;
			this.btn_Luu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Luu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Luu.ForeColor = System.Drawing.Color.White;
			this.btn_Luu.Location = new System.Drawing.Point(722, 482);
			this.btn_Luu.Name = "btn_Luu";
			this.btn_Luu.Size = new System.Drawing.Size(160, 66);
			this.btn_Luu.TabIndex = 12;
			this.btn_Luu.Text = "Lưu";
			this.btn_Luu.UseVisualStyleBackColor = false;
			this.btn_Luu.Click += new System.EventHandler(this.btn_Luu_Click);
			// 
			// btn_Huy
			// 
			this.btn_Huy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Huy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Huy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Huy.ForeColor = System.Drawing.Color.White;
			this.btn_Huy.Location = new System.Drawing.Point(722, 580);
			this.btn_Huy.Name = "btn_Huy";
			this.btn_Huy.Size = new System.Drawing.Size(160, 66);
			this.btn_Huy.TabIndex = 13;
			this.btn_Huy.Text = "Hủy ";
			this.btn_Huy.UseVisualStyleBackColor = false;
			this.btn_Huy.Click += new System.EventHandler(this.btn_Huy_Click);
			// 
			// btn_TroVe
			// 
			this.btn_TroVe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_TroVe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_TroVe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_TroVe.ForeColor = System.Drawing.Color.White;
			this.btn_TroVe.Location = new System.Drawing.Point(903, 482);
			this.btn_TroVe.Name = "btn_TroVe";
			this.btn_TroVe.Size = new System.Drawing.Size(182, 169);
			this.btn_TroVe.TabIndex = 14;
			this.btn_TroVe.Text = "Trở Về ";
			this.btn_TroVe.UseVisualStyleBackColor = false;
			this.btn_TroVe.Click += new System.EventHandler(this.btn_TroVe_Click);
			// 
			// btn_Xoa
			// 
			this.btn_Xoa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Xoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Xoa.ForeColor = System.Drawing.Color.White;
			this.btn_Xoa.Location = new System.Drawing.Point(333, 580);
			this.btn_Xoa.Name = "btn_Xoa";
			this.btn_Xoa.Size = new System.Drawing.Size(160, 66);
			this.btn_Xoa.TabIndex = 15;
			this.btn_Xoa.Text = "Xóa";
			this.btn_Xoa.UseVisualStyleBackColor = false;
			this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
			// 
			// btn_Reload
			// 
			this.btn_Reload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btn_Reload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Reload.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Reload.ForeColor = System.Drawing.Color.White;
			this.btn_Reload.Location = new System.Drawing.Point(525, 580);
			this.btn_Reload.Name = "btn_Reload";
			this.btn_Reload.Size = new System.Drawing.Size(160, 66);
			this.btn_Reload.TabIndex = 16;
			this.btn_Reload.Text = "Reload";
			this.btn_Reload.UseVisualStyleBackColor = false;
			this.btn_Reload.Click += new System.EventHandler(this.btn_Reload_Click);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1090, 49);
			this.panel2.TabIndex = 17;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox1.Image = global::QLKS_ADONET.Properties.Resources.close__1_;
			this.pictureBox1.Location = new System.Drawing.Point(1053, 8);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(30, 31);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 19;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// btnSearch
			// 
			this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(61)))), ((int)(((byte)(161)))));
			this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSearch.ForeColor = System.Drawing.Color.White;
			this.btnSearch.Location = new System.Drawing.Point(966, 60);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(117, 46);
			this.btnSearch.TabIndex = 18;
			this.btnSearch.Text = "Search";
			this.btnSearch.UseVisualStyleBackColor = false;
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// TaoNhanVien
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(1090, 668);
			this.Controls.Add(this.btnSearch);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.btn_Reload);
			this.Controls.Add(this.btn_Xoa);
			this.Controls.Add(this.btn_TroVe);
			this.Controls.Add(this.btn_Huy);
			this.Controls.Add(this.btn_Luu);
			this.Controls.Add(this.txtSearch);
			this.Controls.Add(this.btn_Sua);
			this.Controls.Add(this.btn_Them);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "TaoNhanVien";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "THÊM NHÂN VIÊN";
			this.Load += new System.EventHandler(this.TaoNhanVien_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label lbl_MaNV;
		private System.Windows.Forms.Label lbl_HoTen;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lbl_SDT;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox txt_MatKhau;
		private System.Windows.Forms.TextBox txt_Mail;
		private System.Windows.Forms.TextBox txt_ChucVu;
		private System.Windows.Forms.TextBox txt_SDT;
		private System.Windows.Forms.TextBox txt_DiaChi;
		private System.Windows.Forms.TextBox txt_HoTen;
		private System.Windows.Forms.TextBox txtSearch;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button btn_Them;
		private System.Windows.Forms.Button btn_Sua;
		private System.Windows.Forms.Button btn_Luu;
		private System.Windows.Forms.Button btn_Huy;
		private System.Windows.Forms.Button btn_TroVe;
		private System.Windows.Forms.Button btn_Xoa;
		private System.Windows.Forms.Button btn_Reload;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Label lbMaNhanVien;
	}
}