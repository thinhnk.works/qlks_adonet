﻿using QLKS_ADONET.BS_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_QuanLy
{
	public partial class TaoNhanVien : Form
	{
		bool them;
		BL_NhanVien dbNV = new BL_NhanVien();
		public TaoNhanVien()
		{
			InitializeComponent();
		}
		void LoadData()
		{
			try
			{
				// Đưa dữ liệu lên DataGridView 
				dataGridView1.DataSource = dbNV.LayDSNhanVien();
				// Thay đổi độ rộng cột 
				dataGridView1.AutoResizeColumns();
				// Xóa trống các đối tượng trong Panel 
				// this.txtSearch.ResetText();
				this.txt_HoTen.ResetText();
				this.txt_DiaChi.ResetText();
				this.txt_SDT.ResetText();
				this.txt_Mail.ResetText();
				this.txt_MatKhau.ResetText();
				this.txt_ChucVu.ResetText();
				// Không cho thao tác trên các nút Lưu / Hủy 
				this.btn_Luu.Enabled = false;
				this.btn_Huy.Enabled = false;
				this.panel1.Enabled = false;
				// Cho thao tác trên các nút Thêm / Sửa / Xóa /Thoát 
				this.btn_Them.Enabled = true;
				this.btn_Sua.Enabled = true;
				this.btn_Xoa.Enabled = true;
			}
			catch
			{
				MessageBox.Show("Không lấy được nội dung trong table. Lỗi rồi!!!");
			}
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			// Thứ tự dòng hiện hành 
			int r = dataGridView1.CurrentCell.RowIndex;
			// Chuyển thông tin lên panel 
			this.lbMaNhanVien.Text = dataGridView1.Rows[r].Cells[0].Value.ToString();
			this.txt_HoTen.Text =
			dataGridView1.Rows[r].Cells[1].Value.ToString();
			this.txt_DiaChi.Text =
			dataGridView1.Rows[r].Cells[2].Value.ToString();
			this.txt_SDT.Text =
			dataGridView1.Rows[r].Cells[3].Value.ToString();
			this.txt_Mail.Text =
			dataGridView1.Rows[r].Cells[4].Value.ToString();
			this.txt_ChucVu.Text =
			dataGridView1.Rows[r].Cells[5].Value.ToString();
		}

		private void TaoNhanVien_Load(object sender, EventArgs e)
		{
			LoadData();
		}

		private void btn_Reload_Click(object sender, EventArgs e)
		{
			LoadData();
		}

		private void btn_Them_Click(object sender, EventArgs e)
		{
			them = true;
			// this.txtSearch.ResetText();
			this.txt_HoTen.ResetText();
			this.txt_DiaChi.ResetText();
			this.txt_SDT.ResetText();
			this.txt_Mail.ResetText();
			this.txt_MatKhau.ResetText();
			this.txt_ChucVu.ResetText();
			// Cho thao tác trên các nút Lưu / Hủy / Panel 
			this.btn_Luu.Enabled = true;
			this.btn_Huy.Enabled = true;
			this.panel1.Enabled = true;
			// Không cho thao tác trên các nút Thêm / Xóa / Thoát 
			this.btn_Them.Enabled = false;
			this.btn_Sua.Enabled = false;
			this.btn_Xoa.Enabled = false;
			//this.txt_MaNV.Enabled = false;
			// Đưa con trỏ đến TextField txtThanhPho 
			this.txt_HoTen.Focus();
		}

		private void btn_Sua_Click(object sender, EventArgs e)
		{
			them = false;
			// Cho phép thao tác trên Panel 
			this.panel1.Enabled = true;
			dataGridView1_CellClick(null, null);
			// Cho thao tác trên các nút Lưu / Hủy / Panel 
			this.btn_Luu.Enabled = true;
			this.btn_Huy.Enabled = true;
			this.panel1.Enabled = true;
			// Không cho thao tác trên các nút Thêm / Xóa / Thoát 
			this.btn_Them.Enabled = false;
			this.btn_Sua.Enabled = false;
			this.btn_Xoa.Enabled = false;
			// Đưa con trỏ đến TextField txtMaKH 
			this.txt_HoTen.Focus();
		}

		private void btn_Huy_Click(object sender, EventArgs e)
		{
			// Xóa trống các đối tượng trong Panel 
			//  this.txtSearch.ResetText();
			this.txt_HoTen.ResetText();
			this.txt_DiaChi.ResetText();
			this.txt_SDT.ResetText();
			this.txt_Mail.ResetText();
			this.txt_MatKhau.ResetText();
			this.txt_ChucVu.ResetText();
			// Cho thao tác trên các nút Thêm / Sửa / Xóa / Thoát 
			this.btn_Them.Enabled = true;
			this.btn_Sua.Enabled = true;
			this.btn_Xoa.Enabled = true;
			// Không cho thao tác trên các nút Lưu / Hủy / Panel 
			this.btn_Luu.Enabled = false;
			this.btn_Huy.Enabled = false;
			this.panel1.Enabled = false;
			dataGridView1_CellClick(null, null);
		}

		private void btn_TroVe_Click(object sender, EventArgs e)
		{
			// Khai báo biến traloi 
			DialogResult traloi;
			// Hiện hộp thoại hỏi đáp 
			traloi = MessageBox.Show("Chắc không?", "Trả lời",
			MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
			// Kiểm tra có nhắp chọn nút Ok không? 
			if (traloi == DialogResult.OK) this.Close();
		}

		private void btn_Luu_Click(object sender, EventArgs e)
		{
			if (them)
			{
				try
				{
					// Thực hiện lệnh 
					BL_NhanVien blNV = new BL_NhanVien();
					blNV.ThemNhanVien(this.txt_HoTen.Text, this.txt_DiaChi.Text, this.txt_Mail.Text, this.txt_SDT.Text, this.txt_MatKhau.Text, txt_ChucVu.Text);
					// Load lại dữ liệu trên DataGridView 
					LoadData();
					// Thông báo 
					MessageBox.Show("Đã thêm xong!");
				}
				catch
				{
					MessageBox.Show("Không thêm được. Lỗi rồi!");
				}
			}
			else
			{
				// Thực hiện lệnh 
				BL_NhanVien blNV = new BL_NhanVien();
				blNV.SuaNhanVien(Convert.ToInt32(this.lbMaNhanVien), this.txt_HoTen.Text, this.txt_DiaChi.Text, this.txt_Mail.Text, this.txt_SDT.Text, this.txt_MatKhau.Text, txt_ChucVu.Text);
				// Load lại dữ liệu trên DataGridView 
				LoadData();
				// Thông báo 
				MessageBox.Show("Đã sửa xong!");
			}
			// Đóng kết nối
		}

		private void btn_Xoa_Click(object sender, EventArgs e)
		{
			try
			{
				int r = dataGridView1.CurrentCell.RowIndex;
				// Lấy MaKH của record hiện hành 
				int maNV =
				Convert.ToInt32(dataGridView1.Rows[r].Cells[0].Value.ToString());
				// Viết câu lệnh SQL 
				// Hiện thông báo xác nhận việc xóa mẫu tin 
				// Khai báo biến traloi 
				DialogResult traloi;
				// Hiện hộp thoại hỏi đáp 
				traloi = MessageBox.Show("Chắc xóa mẫu tin này không?", "Trả lời",
				MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				// Kiểm tra có nhắp chọn nút Ok không? 
				if (traloi == DialogResult.Yes)
				{
					dbNV.XoaNhanVien(maNV);
					// Cập nhật lại DataGridView 
					LoadData();
					// Thông báo 
					MessageBox.Show("Đã xóa xong!");
				}
				else
				{
					// Thông báo 
					MessageBox.Show("Không thực hiện việc xóa mẫu tin!");
				}
			}
			catch
			{
				MessageBox.Show("Không xóa được. Lỗi rồi!");
			}
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			if (txtSearch.Text != "")
				dataGridView1.DataSource = dbNV.TimKiemNhanVien(txtSearch.Text.Trim());
			else
			{
				dataGridView1.DataSource = dbNV.LayDSNhanVien();
			}
		}
	}
}
