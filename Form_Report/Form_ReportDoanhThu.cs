﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET.Form_Report
{
	public partial class Form_ReportDoanhThu : Form
	{
		public Form_ReportDoanhThu()
		{
			InitializeComponent();
		}

		private void Form_ReportDoanhThu_Load(object sender, EventArgs e)
		{
            // TODO: This line of code loads data into the 'quanLyKhachSanDataSet.HoaDon' table. You can move, or remove it, as needed.
            this.hoaDonTableAdapter.Fill(this.quanLyKhachSanDataSet.HoaDon);

            this.reportViewer1.RefreshReport();
        }
    }
}
