using QLKS_ADONET.Form_BatDau;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_ADONET
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// new comment
		/// Loi
        /// Xong chuc nang dang nhap
        /// xong quan ly
		/// Xong nhan vien
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form_DangNhap());
		}
	}
}
